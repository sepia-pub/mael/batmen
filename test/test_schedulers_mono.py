#!/usr/bin/env python3

from helper import *
import pandas as pd

def test_fcfs(platform_monoC, workload_static):
    """Tests the scheduler fcfs with different platform and workload files"""
    
    sched = "fcfs"
    test_name = f'{sched}-{platform_monoC.name}-{workload_static.name}'
    output_dir, robin_filename, _ = init_instance(test_name)

    batcmd = gen_batsim_cmd(platform_monoC.filename, workload_static.filename, output_dir, "")
    instance = RobinInstance(output_dir=output_dir,
        batcmd=batcmd,
        schedcmd=f"batmen -v '{sched}'",
        simulation_timeout=30, ready_timeout=5,
        success_timeout=10, failure_timeout=0
    )

    instance.to_file(robin_filename)
    ret = run_robin(robin_filename)

    # Checks that there were no execution error
    assert ret.returncode == 0

    # Checks that the jobs have been executed in FCFS order
    jobs = pd.read_csv(f"{output_dir}/_jobs.csv")
    jobs = jobs[jobs.success==1]
    jobs.sort_values(by="submission_time", inplace=True)
    assert jobs.starting_time.is_monotonic_increasing

def test_easy_bf_small_instance():
    """Tests the scheduler easy backfilling on a small instance"""

    test_name = 'easy_bf_small_instance'
    output_dir, robin_filename, _ = init_instance(test_name)
    
    batcmd = gen_batsim_cmd(
        abspath("test/platforms/monocore/2machines.xml"),
        abspath("test/workloads/static/backfill.json"), output_dir, "")
    instance = RobinInstance(output_dir=output_dir,
        batcmd=batcmd,
        schedcmd="batmen -v 'easy_bf'",
        simulation_timeout=30, ready_timeout=5,
        success_timeout=10, failure_timeout=0
    )
    instance.to_file(robin_filename)
    ret = run_robin(robin_filename)

    # Checks that there were no execution error
    assert ret.returncode == 0

    # Checks that backfilling did occur in this small example
    assert_expected_output(test_name)


def test_easy_bf(platform_monoC, workload_static):
    """Tests the scheduler easy backfilling with different platform and workload files"""

    sched = "easy_bf"
    test_name = f'{sched}-{platform_monoC.name}-{workload_static.name}'
    output_dir, robin_filename, _ = init_instance(test_name)

    batcmd = gen_batsim_cmd(platform_monoC.filename, workload_static.filename, output_dir, "")
    instance = RobinInstance(output_dir=output_dir,
        batcmd=batcmd,
        schedcmd=f"batmen -v '{sched}'",
        simulation_timeout=30, ready_timeout=5,
        success_timeout=10, failure_timeout=0
    )

    instance.to_file(robin_filename)
    ret = run_robin(robin_filename)
    assert ret.returncode == 0