from helper import *


def test_expected_output(test_with_expected_log):
    """Test for each expected log (thanks to pytest fixtures) that the test was 
    run and the _jobs.csv outputs are strictly equal"""

    assert_expected_output(test_with_expected_log)

def test_expected_behavior(test_with_expected_behavior) :
    """Test for each expected log (thanks to pytest fixtures) that the test was
    run and the user_behavior_stats.csv outputs are strictly equal if they both 
    exists"""
    assert_expected_behavior(test_with_expected_behavior)
