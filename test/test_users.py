from helper import *
import csv
Workload = namedtuple('Workload', ['name', 'filename'])

users = [
    "dicho_intersubmission_time",
    "routine_greedy",
    "replay_user_rigid",
]

#### User models ####


def test_dicho_intersubmission_time(platform_multiC):
    run_user("dicho_intersubmission_time", platform_multiC)


def test_routine_greedy(platform_multiC):
    run_user("routine_greedy", platform_multiC)


def test_think_time_user(platform_multiC):
    run_user("think_time_user", platform_multiC)


def test_cocktail_user_model(platform_multiC):
    run_user("cocktail_user_model", platform_multiC)

#### Replay users ####


def test_replay_user_rigid(platform_multiC):
    out_dir = run_user('replay_user_rigid', platform_multiC)
    assert_exec_time_equals_profile(out_dir)


def run_user(user_name, platform_multiC, test_name=None, schedconf=None):
    if test_name == None:
        test_name = f'{user_name}-{platform_multiC.name}'
    if schedconf == None:
        schedconf = f"test/schedconf/{user_name}.json"
    output_dir, robin_filename, _ = init_instance(test_name)

    batcmd = gen_batsim_cmd(platform_multiC.filename, empty_workload, output_dir,
                            "--energy  --enable-compute-sharing --enable-dynamic-jobs --acknowledge-dynamic-jobs --enable-profile-reuse")
    instance = RobinInstance(output_dir=output_dir,
                             batcmd=batcmd,
                             schedcmd=f"batmen -v bin_packing --variant_options_filepath {schedconf}",
                             simulation_timeout=30, ready_timeout=5,
                             success_timeout=10, failure_timeout=0
                             )

    instance.to_file(robin_filename)
    ret = run_robin(robin_filename)
    assert ret.returncode == 0

    if has_expected_output(test_name):
        assert_expected_output(test_name)
    if has_expected_behavior(test_name):
        assert_expected_behavior(test_name)
    
    return output_dir


def assert_exec_time_equals_profile(dir):
    filename = f"{dir}/_jobs.csv"
    with open(filename) as csvfile:
        reader = csv.reader(csvfile)
        header = next(reader)
        for row in reader:
            exec_time = row[9]
            if exec_time != "":
                profile = float(row[2])
                exec_time = float(exec_time)
                assert profile == exec_time, f"Error in {filename}, line {reader.line_num}. Fileds `execution_time` and `profile` should be equal."
