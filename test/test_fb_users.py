from helper import *
import json
from test_users import run_user, assert_exec_time_equals_profile
from distance_batsim_output import distances

##### Utils... #####
Workload = namedtuple('Workload', ['name', 'filename'])
Platform = namedtuple('Platform', ['name', 'filename'])
schedconf_file = 'test-instances/user_description_file.json'

two_machine_platform = Platform(
    name='two_machine_platform', filename='test/platforms/multicore/2machines.xml')
mc_europar_platform = Platform(
    name='mc_europar_platform', filename='test/platforms/mc_europar.xml')


def make_fb_user_description_file(user_name, user_category, input_json):
    assert len(user_name)==len(user_category)==len(input_json), "The three lists should be of the same size"

    user_description = {
        "users": [
            {
                "name": name,
                "category": cat,
                "param": {
                    "input_json": json
                }
            }
            for name, cat, json in zip(user_name, user_category, input_json)
        ]
    }
    with open(schedconf_file, 'w') as user_description_file:
        json.dump(user_description, user_description_file)


def launch_fb_test_1user(user_category, test_input):
    make_fb_user_description_file(
        user_name=[user_category],
        user_category=[user_category],
        input_json=[f"test/workloads/SABjson/{test_input}.SABjson"])

    out_dir = run_user(user_category, two_machine_platform,
                       test_name=f'{user_category}-{test_input}', schedconf=schedconf_file)
    assert_exec_time_equals_profile(out_dir)


##### Tests edge cases #####
def test_tt_only_no_session():
    """Empty SABjson."""
    launch_fb_test_1user(user_category='fb_user_think_time_only',
                   test_input='no_session')


def test_tt_only_one_session_many_jobs():
    """One session that contains ten jobs."""
    launch_fb_test_1user(user_category='fb_user_think_time_only',
                   test_input='one_session_many_jobs')


def test_tt_only_many_start_sessions():
    """Five start sessions with no dependency between them."""
    launch_fb_test_1user(user_category='fb_user_think_time_only',
                   test_input='many_start_sessions')


def test_tt_only_zero_think_time():
    """Two sessions following each other, with a think time of 0s between them."""
    launch_fb_test_1user(user_category='fb_user_think_time_only',
                   test_input='zero_think_time')


def test_tt_only_many_following():
    """A session (session 1) has five followers, four with a think time of 1000s and one with a think time of 2000s."""
    launch_fb_test_1user(user_category='fb_user_think_time_only',
                   test_input='many_following')


def test_tt_only_many_preceding():
    """A session (session 6) depends on five others, with a think time of 1000s."""
    launch_fb_test_1user(user_category='fb_user_think_time_only',
                   test_input='many_preceding')


def test_tt_only_fully_loaded_platform():
    """Test feedback when platform is fully loaded."""
    launch_fb_test_1user(user_category='fb_user_think_time_only',
                   test_input='load_platform')


def test_simultaneous_active_sessions():
    """Test when there are 2 simulataneous active sessions for one user.
    See issue#8: https://gitlab.irit.fr/sepia-pub/mael/batmen/-/issues/8"""
    launch_fb_test_1user(user_category='fb_user_think_time_only',
                   test_input='simultaneous_active_sessions')


##### Tests integration #####
def test_tt_only_simple_workload():
    """A simple SAB json. See diagram https://app.diagrams.net/#G1tbo7oHahsgxTmhICucCGam5XNtshOUOb"""
    launch_fb_test_1user(user_category='fb_user_think_time_only',
                   test_input='proto_SABjson_v2')

def simu_output_are_close_enough(out_dir1, out_dir2):
    """Returns true if the jobs.csv in both directories are epsilon-close (for 
    the normalized euclidean distance) for each of the fields submission time, 
    start time and finish time."""
    epsilon = 1e-4 # threshold under which we consider that the distance is 
                   # neglectable compared to the summed execution times

    dis = distances(f"{out_dir1}/_jobs.csv", f"{out_dir2}/_jobs.csv", 
                    euclidean=False, norm_eucl=True,
                    field=["submission_time", "starting_time", "finish_time"])

    return dis["submission_time"]["normalized_euclidean"] < epsilon and (
        dis["starting_time"]["normalized_euclidean"] < epsilon) and (
        dis["finish_time"]["normalized_euclidean"] < epsilon)


def test_tt_only_multiuser():
    """Workload: 4 users taken from the 10 first days of metacentrum log.
    This tests checks:
    - that the simulation with feedback doesn't fail for multiuser instances
    - that the simulation outputs are "very close" to the rigid replay, no 
      matter the delimitation approach, because the platform is oversized.
    To test the "closeness" we make use of custom distances (see batmen-tools).
    """
    names = ["user5", "user9", "user10", "user11"]
    
    # Launch a simulation with replay rigid, for comparison
    category = "replay_user_rigid"
    test_name = "mc_10days"
    jsons = [f"test/workloads/dyn/{user}.json" for user in names]
    make_fb_user_description_file(
        user_name=names, user_category=[category]*4, input_json=jsons)
    rigid_dir = run_user(category, mc_europar_platform,
                         test_name=f'{category}-{test_name}', schedconf=schedconf_file)


    # Launch feedback users on the same original log, and compare with rigid
    category = "fb_user_think_time_only"

    # mc_10_days_a60
    test_name = "mc_10days_a60"
    jsons = [f"test/workloads/SABjson/{test_name}/{user}.SABjson" for user in names]
    make_fb_user_description_file(
        user_name=names, user_category=[category]*4, input_json=jsons)
    feedback_dir = run_user(category, mc_europar_platform,
                            test_name=f'{category}-{test_name}', schedconf=schedconf_file)
    assert simu_output_are_close_enough(feedback_dir, rigid_dir)

    # mc_10_days_l60
    test_name = "mc_10days_l60"
    jsons = [f"test/workloads/SABjson/{test_name}/{user}.SABjson" for user in names]
    make_fb_user_description_file(
        user_name=names, user_category=[category]*4, input_json=jsons)
    run_user(category, mc_europar_platform,
             test_name=f'{category}-{test_name}', schedconf=schedconf_file)
    assert simu_output_are_close_enough(feedback_dir, rigid_dir)


    # mc_10_days_m60
    test_name = "mc_10days_m60"
    jsons = [f"test/workloads/SABjson/{test_name}/{user}.SABjson" for user in names]
    make_fb_user_description_file(
        user_name=names, user_category=[category]*4, input_json=jsons)
    run_user(category, mc_europar_platform,
             test_name=f'{category}-{test_name}', schedconf=schedconf_file)
    assert simu_output_are_close_enough(feedback_dir, rigid_dir)


def test_tt_only_multiuser_undersized_platform():
    """Same test than the previous one, but with an undersized plateform."""
    names = ["user5", "user9", "user10", "user11"]
    
    # Launch a simulation with replay rigid, for comparison
    category = "replay_user_rigid"
    test_name = "mc_10days"
    jsons = [f"test/workloads/dyn/{user}.json" for user in names]
    make_fb_user_description_file(
        user_name=names, user_category=[category]*4, input_json=jsons)
    rigid_dir = run_user(category, two_machine_platform,
                         test_name=f'{category}-{test_name}_undersized', schedconf=schedconf_file)


    # Launch feedback users on the same original log, and compare with rigid
    category = "fb_user_think_time_only"

    # mc_10_days_a60
    test_name = "mc_10days_a60"
    jsons = [f"test/workloads/SABjson/{test_name}/{user}.SABjson" for user in names]
    make_fb_user_description_file(
        user_name=names, user_category=[category]*4, input_json=jsons)
    feedback_dir = run_user(category, two_machine_platform,
                            test_name=f'{category}-{test_name}_undersized', schedconf=schedconf_file)
    assert not simu_output_are_close_enough(feedback_dir, rigid_dir)

    # mc_10_days_l60
    test_name = "mc_10days_l60"
    jsons = [f"test/workloads/SABjson/{test_name}/{user}.SABjson" for user in names]
    make_fb_user_description_file(
        user_name=names, user_category=[category]*4, input_json=jsons)
    run_user(category, two_machine_platform,
             test_name=f'{category}-{test_name}_undersized', schedconf=schedconf_file)
    assert not simu_output_are_close_enough(feedback_dir, rigid_dir)


    # mc_10_days_m60
    test_name = "mc_10days_m60"
    jsons = [f"test/workloads/SABjson/{test_name}/{user}.SABjson" for user in names]
    make_fb_user_description_file(
        user_name=names, user_category=[category]*4, input_json=jsons)
    run_user(category, two_machine_platform,
             test_name=f'{category}-{test_name}_undersized', schedconf=schedconf_file)
    assert not simu_output_are_close_enough(feedback_dir, rigid_dir)


    
