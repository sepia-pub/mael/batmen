from helper import *
from test_users import run_user, assert_exec_time_equals_profile
Workload = namedtuple('Workload', ['name', 'filename'])
Platform = namedtuple('Platform', ['name', 'filename'])


two_machine_platform = Platform(name='two_machine_platform', filename='test/platforms/multicore/2machines.xml')

users = [
    "dm_user_reconfig",
    "dm_user_degrad_space", 
    "dm_user_degrad_temp", 
    "dm_user_renounce",
    "dm_user_delay",
    "dm_user_multi_behavior",
    "dm_user_multi_behavior_yellow",
    "dm_user_multi_behavior_C_you_later_only"
]



def test_dm_user_delay(platform_multiC):
    """ Test user_class dm_user_delay as described in the file schedconf/dm_user_delay"""
    out_dir = run_user("dm_user_delay", platform_multiC)
    assert_exec_time_equals_profile(out_dir)

def test_dm_user_renounce(platform_multiC):
    """ Test user_class dm_user_renounce as described in the file schedconf/dm_user_renounce"""
    out_dir = run_user("dm_user_renounce", platform_multiC)
    assert_exec_time_equals_profile(out_dir)

def test_dm_user_reconfig(platform_multiC):
    """ Test user_class dm_user_reconfig as described in the file schedconf/dm_user_reconfig"""
    run_user("dm_user_reconfig", platform_multiC)

def test_dm_user_degrad_space(platform_multiC):
    """ Test user_class dm_user_degrad_space as described in the file schedconf/dm_user_degrad_space"""
    run_user("dm_user_degrad_space", platform_multiC)

def test_dm_user_degrad_temp(platform_multiC):
    """Test user_class dm_user_degrad_temp as described in the file schedconf/dm_user_degrad_temp"""
    run_user("dm_user_degrad_temp", platform_multiC)

def test_speedup():
    """ Test user_class dm_user_reconfig alpha speedup as described in the file schedconf/reconfig_alpha.json"""
    run_user("reconfig_alpha", two_machine_platform)


