from helper import *

def test_multicore_filler(platform_multiC, workload_static):
    """Tests the scheduler mutlicore_filler with different platform and workload files"""

    sched = "multicore_filler"

    test_name = f'{sched}-{platform_multiC.name}-{workload_static.name}'
    output_dir, robin_filename, _ = init_instance(test_name)

    batcmd = gen_batsim_cmd(platform_multiC.filename, workload_static.filename, output_dir, "--energy")
    instance = RobinInstance(output_dir=output_dir,
        batcmd=batcmd,
        schedcmd=f"batmen -v {sched}",
        simulation_timeout=30, ready_timeout=5,
        success_timeout=10, failure_timeout=0
    )

    instance.to_file(robin_filename)
    ret = run_robin(robin_filename)
    assert ret.returncode == 0

    if has_expected_output(test_name):
        assert_expected_output(test_name)


def test_bin_packing(platform_multiC, workload_static):
    """Tests the scheduler bin_packing with different platform and workload files"""

    sched = "bin_packing"
    test_name = f'{sched}-{platform_multiC.name}-{workload_static.name}'
    output_dir, robin_filename, _ = init_instance(test_name)

    batcmd = gen_batsim_cmd(platform_multiC.filename, workload_static.filename, output_dir, "--energy --enable-compute-sharing")
    instance = RobinInstance(output_dir=output_dir,
        batcmd=batcmd,
        schedcmd=f"batmen -v {sched}",
        simulation_timeout=30, ready_timeout=5,
        success_timeout=10, failure_timeout=0
    )

    instance.to_file(robin_filename)
    ret = run_robin(robin_filename)
    assert ret.returncode == 0

    if has_expected_output(test_name):
        assert_expected_output(test_name)


def test_bin_packing_energy(platform_multiC, workload_static):
    """Tests the scheduler bin_packing_energy with different platform and workload files"""

    sched = "bin_packing_energy"
    test_name = f'{sched}-{platform_multiC.name}-{workload_static.name}'
    output_dir, robin_filename, _ = init_instance(test_name)

    batcmd = gen_batsim_cmd(platform_multiC.filename, workload_static.filename, output_dir, "--energy --enable-compute-sharing")
    instance = RobinInstance(output_dir=output_dir,
        batcmd=batcmd,
        schedcmd=f"batmen -v {sched}",
        simulation_timeout=30, ready_timeout=5,
        success_timeout=10, failure_timeout=0
    )

    instance.to_file(robin_filename)
    ret = run_robin(robin_filename)
    assert ret.returncode == 0

    if has_expected_output(test_name):
        assert_expected_output(test_name)
