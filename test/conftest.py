#!/usr/bin/env python3
import glob
import pytest
import subprocess
from collections import namedtuple
from os.path import abspath, basename
from os import listdir

Workload = namedtuple('Workload', ['name', 'filename'])
Platform = namedtuple('Platform', ['name', 'filename'])
Scheduler = namedtuple('Scheduler', ['name', 'short_name'])


def pytest_generate_tests(metafunc):
    if 'platform_monoC' in metafunc.fixturenames:
        platform_files = glob.glob('test/platforms/monocore/*.xml')
        platforms = [Platform(
            name=basename(platform_file).replace('.xml', ''),
            filename=abspath(platform_file)) for platform_file in platform_files]
        metafunc.parametrize('platform_monoC', platforms)

    if 'workload_static' in metafunc.fixturenames:
        workload_files = [
            "batpaper",
            "mixed", 
            "para_homo"
        ]
        path = 'test/workloads/static'
        workloads = [Workload(name=wl, filename=abspath(f"{path}/{wl}.json")) 
                     for wl in workload_files]
        metafunc.parametrize('workload_static', workloads)

    if 'platform_multiC' in metafunc.fixturenames:
        platform_files = glob.glob('test/platforms/multicore/*.xml')
        platforms = [Platform(
            name=basename(platform_file).replace('.xml', ''),
            filename=abspath(platform_file)) for platform_file in platform_files]
        metafunc.parametrize('platform_multiC', platforms)

    if 'sched_mono' in metafunc.fixturenames:
        scheds = [
            Scheduler('easy_bf', 'easy'),
            Scheduler('fcfs', 'fcfs')
        ]
        metafunc.parametrize('sched_mono', scheds)

    if 'sched_multi' in metafunc.fixturenames:
        scheds = [
            Scheduler('multicore_filler', 'filler'), 
            Scheduler('bin_packing', 'bp'),
            Scheduler('bin_packing_energy', 'bpNRJ')
        ]
        metafunc.parametrize('sched_multi', scheds)

    if 'test_with_expected_log' in metafunc.fixturenames:
        instance_files = glob.glob('test/expected_log/*')
        instances = [basename(instance_file).replace('_jobs.csv', '')
            for instance_file in instance_files]
        metafunc.parametrize('test_with_expected_log', instances)
        
    if 'test_with_expected_behavior' in metafunc.fixturenames:
        instance_files = glob.glob('test/expected_behavior_log/*')
        instances = [basename(instance_file).replace('_user_stats_behaviors.csv', '')
            for instance_file in instance_files]
        metafunc.parametrize('test_with_expected_behavior', instances)

# def pytest_cmdline_preparse(config, args):
#     html_file = "test-out/testreport.html"
#     print('HTML report file:', html_file)
#     args.extend(['--html', html_file, '--self-contained-html'])
