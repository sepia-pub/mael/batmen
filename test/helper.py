#!/usr/bin/env python3
import os
import os.path
import subprocess
import filecmp
from collections import namedtuple
from os.path import abspath
import pandas as pd


empty_workload = abspath('test/workloads/static/empty.json')

class RobinInstance(object):
    def __init__(self, output_dir, batcmd, schedcmd, simulation_timeout, ready_timeout, success_timeout, failure_timeout):
        self.output_dir = output_dir
        self.batcmd = batcmd
        self.schedcmd = schedcmd
        self.simulation_timeout = simulation_timeout
        self.ready_timeout = ready_timeout
        self.success_timeout = success_timeout
        self.failure_timeout = failure_timeout

    def to_yaml(self):
        # Manual dump to avoid dependencies
        return f'''output-dir: '{self.output_dir}'
batcmd: "{self.batcmd}"
schedcmd: "{self.schedcmd}"
simulation-timeout: {self.simulation_timeout}
ready-timeout: {self.ready_timeout}
success-timeout: {self.success_timeout}
failure-timeout: {self.failure_timeout}
'''

    def to_file(self, filename):
        create_dir_rec_if_needed(os.path.dirname(filename))
        write_file(filename, self.to_yaml())

def gen_batsim_cmd(platform, workload, output_dir, more_flags):
    return f"batsim -p '{platform}' -w '{workload}' -e '{output_dir}/' {more_flags}"

def write_file(filename, content):
    file = open(filename, "w")
    file.write(content)
    file.close()

def create_dir_rec_if_needed(dirname):
    if not os.path.exists(dirname):
            os.makedirs(dirname)

def run_robin(filename):
    return subprocess.run(['robin', filename])

def init_instance(test_name):
    output_dir = os.path.abspath(f'test-out/{test_name}')
    robin_filename = os.path.abspath(f'test-instances/{test_name}.yaml')
    schedconf_filename = f'{output_dir}/schedconf.json'

    create_dir_rec_if_needed(output_dir)

    return (output_dir, robin_filename, schedconf_filename)

def has_expected_output(test_file):
    return os.path.exists('test/expected_log/' + test_file + '_jobs.csv')

def assert_expected_output(test_file):
    expected = 'test/expected_log/' + test_file + '_jobs.csv'
    obtained = 'test-out/' +  test_file + '/_jobs.csv'
    assert filecmp.cmp(expected, obtained), f"\
        Files {expected} and {obtained} should be equal but are not.\n\
        Run `diff {expected} {obtained}` to investigate why.\n\
        Run `cp {obtained} {expected}` to override the expected file with the obtained."
    
def has_expected_behavior(test_file):
    return os.path.exists(f"test/expected_behavior_log/{test_file}_user_stats_behaviors.csv")

def assert_expected_behavior(test_file) :
    expected = f"test/expected_behavior_log/{test_file}_user_stats_behaviors.csv"
    obtained = f"test-out/{test_file}/user_stats_behaviors.csv"
    if os.path.exists(expected) :
        assert filecmp.cmp(expected, obtained), f"\
        Files {expected} and {obtained} should be equal but are not.\n\
        Run `diff {expected} {obtained}` to investigate why.\n\
        Run `cp {obtained} {expected}` to override the expected file with the obtained."

def assert_same_outputs(expected, obtained):
    """Test that the relevent columns of the _jobs.csv are identical"""
    cols = ["job_id", "submission_time", "final_state", "starting_time", "execution_time"]

    df_o = pd.read_csv(obtained)[cols]
    df_e = pd.read_csv(expected)[cols]
    
    assert df_o.equals(df_e), f"\
        Files {expected} and {obtained} should be equal but are not.\n\
        Run `diff {expected} {obtained}` to investigate why."