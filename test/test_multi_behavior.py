from helper import *
from test_users import run_user
import json

Workload = namedtuple('Workload', ['name', 'filename'])
Platform = namedtuple('Platform', ['name', 'filename'])
two_machine_platform = Platform(name='two_machine_platform', filename='test/platforms/multicore/2machines.xml')


def make_monolithic_file(user_name, input_json, probability_name, seed=None, red_windows=None, yellow_windows=None):
    """Utility function to create a user_description file with the given user_names, input_jsons, probability_name etc."""

    assert len(user_name) == len(input_json), "The two lists should be of the same size"
    schedconf_file = 'test-instances/user_description_file.json'
    proba_dict = {probability_name + "_multi_core": 1.0, probability_name + "_mono_core" : 1.0}
    if probability_name in ["red_prob_degrad","yellow_prob_degrad", "red_prob_reconfig","yellow_prob_reconfig"]:
        proba_dict = {probability_name + "_multi_core" : 1.0, "red_prob_rigid_mono_core" : 1.0,
                      "yellow_prob_rigid_mono_core" : 1.0}
    error_file = {
        "users": [
            {
                "name": name,
                "category": "dm_user_multi_behavior",
                "param": {
                    "input_json": input_json_user,
                    **proba_dict
                }
            }
            for name, input_json_user in zip(user_name, input_json)
        ]
    }
    if seed:
        error_file["seed"] = seed
    if red_windows:
        error_file["red_windows"] = red_windows
    if yellow_windows:
        error_file["yellow_windows"] = yellow_windows
    with open(schedconf_file, 'w+') as error_description_file:
        json.dump(error_file, error_description_file)

def monolithic_tests(proba_names,names,input_json,seed=None,red_windows=None,yellow_windows=None) :
    """Utility function to launch tests with users who do only one behavior in windows and compare the output to
    their dm_user_replay class equivalent"""
    schedconf_file = 'test-instances/user_description_file.json'
    for proba_name in proba_names:
        behavior_name = proba_name.split("_")[-1]
        make_monolithic_file(names, input_json, proba_name, seed=seed, red_windows=red_windows,
                             yellow_windows=yellow_windows)
        test_file_name = f'dm_user_multi_behavior_{proba_name}_only'
        out_dir = run_user(test_file_name, two_machine_platform,
                           test_name=test_file_name, schedconf=schedconf_file)
        if not ("comm" in out_dir):
            content_job = open(out_dir + "/_jobs.csv").readlines()
        if not ("rigid" in out_dir or "degrad" in out_dir):
            expected_log = open(f"test/expected_log/dm_user_{behavior_name}-2machines_jobs.csv").readlines()
        elif "degrad" in out_dir :
            expected_log = open(f"test/expected_log/dm_user_{behavior_name}_space-2machines_jobs.csv").readlines()
        else:
            expected_log = open(f"test/expected_log/replay_user_rigid-2machines_jobs.csv").readlines()
        assert content_job == expected_log

def test_dm_user_multi_behavior(platform_multiC):
    """ test dm_user_multi_behavior class and check that writing [200000,300000] as an unique interval
    or a sorted list of disjoint interval has the same effect. Allowing us to check that the windows union operation is
    done correctly"""
    out_dir_1 = run_user("dm_user_multi_behavior", platform_multiC)
    out_dir_2 = run_user("dm_user_multi_behavior_many_windows", platform_multiC)
    with open(f"{out_dir_1}/_jobs.csv") as job_file_1:
        job_line_1 = job_file_1.readlines()
    with open(f"{out_dir_2}/_jobs.csv") as job_file_2:
        job_line_2 = job_file_2.readlines()
    assert job_line_1 == job_line_2


def test_dm_user_multi_behavior_yellow(platform_multiC):
    """ test dm_user_multi_behavior with yellow_windows"""
    run_user("dm_user_multi_behavior_yellow", platform_multiC)


def test_dm_user_multi_behavior_C_you_later_only(platform_multiC):
    """ Test users that only do see_you_later behavior in red_windows"""
    run_user("dm_user_multi_behavior_C_you_later_only", platform_multiC)
    run_user("dm_user_multi_behavior_C_you_later_only_two_jobs", platform_multiC)


def test_dm_user_multi_behavior_mono_behavior_red(platform_multiC):
    """ Test users that only do one behavior in red_window"""
    proba_names = ["red_prob_degrad", "red_prob_rigid", "red_prob_reconfig", "red_prob_renounce"]
    names = ["user" + str(i + 14) for i in range(3)] + ["user18"]
    input_json = ["test/workloads/dyn/user" + str(i + 14) + ".json" for i in range(3)] + [
        "test/workloads/dyn/user18.json"]
    monolithic_tests(proba_names, names, input_json, seed=3, red_windows=[[200000, 300000]])


def test_dm_user_multi_behavior_mono_behavior_yellow(platform_multiC):
    """ Test users that only do one behavior in yellow_windows"""
    proba_names = ["yellow_prob_reconfig", "yellow_prob_degrad", "yellow_prob_rigid"]
    names = ["user" + str(i + 14) for i in range(3)] + ["user18"]
    input_json = ["test/workloads/dyn/user" + str(i + 14) + ".json" for i in range(3)] + [
        "test/workloads/dyn/user18.json"]
    monolithic_tests(proba_names, names, input_json, seed=3, yellow_windows=[[200000, 300000]])


def test_dm_user_small_workload(platform_multiC):
    """ test multi_behavior on small workload file (<100 jobs) for human checkable log"""
    for i in range(1, 6):
        run_user(f"dm_user_multi_behavior_small_workload_{i}", platform_multiC)
