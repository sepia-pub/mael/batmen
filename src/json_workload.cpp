#include "json_workload.hpp"
#include "queue.hpp"

#include <cmath>
#include <fstream>
#include <limits>
#include <list>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include "pempek_assert.hpp"
#include "rapidjson/rapidjson.h"
#include <loguru.hpp>

using namespace rapidjson;
using namespace std;

Session::Session(
    unsigned long session_id, long first_submit, unsigned long nb_jobs)
{
    id = session_id;
    original_first_submit_time = first_submit;
    start_time = DATE_UNKNOWN;
    unfinished_jobs = nb_jobs;
    preceding_sessions = map<Session *, double>();
    following_sessions = set<Session *>();
    jobs = list<const Job *>();
}

Session::~Session()
{
    for (const Job *job : jobs)
    {
        delete job;
    }
    jobs.clear();
    preceding_sessions.clear();
    following_sessions.clear();
}

Workload::~Workload()
{
    deallocate();
}

Job *Workload::operator[](string jobID)
{
    PPK_ASSERT_ERROR(
        _jobs.count(jobID) == 1, "Job '%s' does not exist", jobID.c_str());
    return _jobs.at(jobID);
}

const Job *Workload::operator[](string jobID) const
{
    return _jobs.at(jobID);
}

int Workload::nb_jobs() const
{
    return (int)_jobs.size();
}

void Workload::set_rjms_delay(Rational rjms_delay)
{
    PPK_ASSERT_ERROR(rjms_delay >= 0);
    _rjms_delay = rjms_delay;
}

void Workload::add_job_from_json_object(
    const Value &object, const string &job_id, double submission_time)
{
    Job *job = job_from_json_object(object);
    job->id = job_id;
    job->submission_time = submission_time;

    // Let's apply the RJMS delay on the job
    job->walltime += _rjms_delay;

    PPK_ASSERT_ERROR(_jobs.count(job_id) == 0,
        "Job '%s' already exists in the Workload", job_id.c_str());
    _jobs[job_id] = job;
}

void Workload::add_job_from_json_description_string(
    const string &json_string, const string &job_id, double submission_time)
{
    Job *job = job_from_json_description_string(json_string);
    job->id = job_id;
    job->submission_time = submission_time;

    // Let's apply the RJMS delay on the job
    job->walltime += _rjms_delay;

    PPK_ASSERT_ERROR(_jobs.count(job_id) == 0,
        "Job '%s' already exists in the Workload", job_id.c_str());
    _jobs[job_id] = job;
}

void Workload::put_file_into_buffer(const string &filename)
{
    std::ifstream file(filename, std::ios::binary);
    if (!file.is_open())
        throw runtime_error("Cannot open file '" + filename + "'");

    file.seekg(0, std::ios::end);
    std::streamsize size = file.tellg();
    file.seekg(0, std::ios::beg);

    vector<char> buffer(size);
    if (!file.read(buffer.data(), size))
        throw runtime_error("Cannot read file '" + filename + "'");

    _fileContents = new char[size + 1];
    memcpy(_fileContents, buffer.data(), size);
    _fileContents[size] = '\0';
}

void Workload::deallocate()
{
    // Let's delete all allocated jobs
    for (auto &mit : _jobs)
    {
        delete mit.second;
    }

    _jobs.clear();

    delete _fileContents;
    _fileContents = nullptr;
}

Job *Workload::job_from_json_description_string(const string &json_string)
{
    Document document;

    if (document.Parse(json_string.c_str()).HasParseError())
        throw runtime_error("Invalid json string '" + json_string + "'");

    return job_from_json_object(document);
}

Job *Workload::job_from_json_object(const Value &object)
{
    PPK_ASSERT_ERROR(object.IsObject(), "Invalid json object: not an object");

    PPK_ASSERT_ERROR(
        object.HasMember("id"), "Invalid json object: no 'id' member");
    PPK_ASSERT_ERROR(object["id"].IsString(),
        "Invalid json object: 'id' member is not a string");
    PPK_ASSERT_ERROR(
        object.HasMember("res"), "Invalid json object: no 'res' member");
    PPK_ASSERT_ERROR(object["res"].IsInt(),
        "Invalid json object: 'res' member is not an integer");
    PPK_ASSERT_ERROR(object.HasMember("profile"),
        "Invalid json object: no 'profile' member");
    PPK_ASSERT_ERROR(object["profile"].IsString(),
        "Invalid json object: 'profile' member is not a string");

    Job *j = new Job;
    j->id = object["id"].GetString();
    j->walltime = -1;
    j->has_walltime = true;
    j->nb_requested_resources = object["res"].GetInt();
    j->profile = object["profile"].GetString();

    if (object.HasMember("walltime"))
    {
        PPK_ASSERT_ERROR(object["walltime"].IsNumber(),
            "Invalid json object: 'walltime' member is not a number");
        j->walltime = object["walltime"].GetDouble();
    }

    PPK_ASSERT_ERROR(j->walltime == -1 || j->walltime > 0,
        "Invalid json object: 'walltime' should either be -1 (no walltime) "
        "or strictly positive.");

    if (j->walltime == -1)
        j->has_walltime = false;

    return j;
}

bool JobComparator::operator()(const Job *j1, const Job *j2) const
{
    PPK_ASSERT_ERROR(j1->id.find('!') != string::npos,
        "I thought jobID had always a form 'wl!id'...");

    string id1 = j1->id.substr(j1->id.find('!') + 1, j1->id.size());
    string id2 = j2->id.substr(j2->id.find('!') + 1, j2->id.size());

    if (id1 == id2) /* different workload, same job number: comp on wl name */
        return j1->id < j2->id;
    if (id1.length() == id2.length()) /* same length: lexicographic order */
        return id1 < id2;
    return id1.length() < id2.length();
}

bool JobComparator::operator()(Job j1, Job j2) const
{
    return JobComparator::operator()(&j1, &j2);
}

bool JobSubmissionOrder::operator()(const Job *j1, const Job *j2) const
{
    if (j1->submission_time == j2->submission_time)
        return jobcmp(j1, j2);
    else
        return j1->submission_time < j2->submission_time;
}

bool SessionComparator::operator()(const Session *s1, const Session *s2) const
{
    PPK_ASSERT_ERROR(
        s1->start_time != DATE_UNKNOWN && s2->start_time != DATE_UNKNOWN);

    return s1->start_time > s2->start_time;
}

UserSubmitQueue::UserSubmitQueue(
    const std::string user_name, const std::string input_json)
{
    /* Parse the JSON input... */
    Document doc = Parser::parse_input_json(input_json);
    string error_prefix = "Invalid JSON file '" + input_json + "'";

    PPK_ASSERT_ERROR(
        doc.IsObject(), "%s: not a JSON object", error_prefix.c_str());

    PPK_ASSERT_ERROR(doc.HasMember("jobs"), "%s: the 'jobs' array is missing",
        error_prefix.c_str());
    const Value &jobs = doc["jobs"];
    PPK_ASSERT_ERROR(jobs.IsArray(), "%s: the 'jobs' member is not an array",
        error_prefix.c_str());

    for (SizeType i = 0; i < jobs.Size(); i++)
    {
        const Value &job_json_description = jobs[i];

        Job *j = Parser::job_from_json_object(user_name, job_json_description);
        _jobs.push_back(j);
    }

    /* Sort the list by submission time */
    _jobs.sort(submission_order);
}

UserSubmitQueue::~UserSubmitQueue()
{
    for (auto j : _jobs)
    {
        delete j;
    }
}

void UserSubmitQueue::insert_sorted(const Job *j)
{
    Job *job = new Job;
    *job = *j;
    auto start = _jobs.begin();
    auto end = _jobs.end();
    while (start != end && submission_order((*start), job))
    {
        ++start;
    }
    _jobs.insert(start, job);
}

const Job *UserSubmitQueue::top() const
{
    return _jobs.front();
}

const Job *UserSubmitQueue::pop()
{
    Job *j = _jobs.front();
    _jobs.pop_front();
    return j;
}

bool UserSubmitQueue::is_empty() const
{
    return _jobs.size() == 0;
}

// Mainly copy-paste from the 'load_from_json' methods from the classes
// Workload, Jobs and Profiles in batsim
Document Parser::parse_input_json(const string input_json)
{
    LOG_F(1, "Loading JSON workload '%s'...", input_json.c_str());
    string error_prefix = "Invalid JSON file '" + input_json + "'";

    // Let the file content be placed in a string
    ifstream ifile(input_json);
    PPK_ASSERT_ERROR(
        ifile.is_open(), "%s: cannot read file", error_prefix.c_str());
    string content;

    ifile.seekg(0, ios::end);
    content.reserve(static_cast<unsigned long>(ifile.tellg()));
    ifile.seekg(0, ios::beg);

    content.assign((std::istreambuf_iterator<char>(ifile)),
        std::istreambuf_iterator<char>());

    // JSON document creation
    Document doc;
    doc.Parse(content.c_str());
    PPK_ASSERT_ERROR(
        !doc.HasParseError(), "%s: could not be parsed", error_prefix.c_str());

    return doc;
}

Session *Parser::session_graph_from_SABjson(
    const string user_name, const string input_json)
{
    Document doc = Parser::parse_input_json(input_json);
    string error_prefix = "Invalid SABjson file '" + input_json + "'";

    PPK_ASSERT_ERROR(
        doc.IsObject(), "%s: not a JSON object", error_prefix.c_str());

    PPK_ASSERT_ERROR(doc.HasMember("sessions"),
        "%s: the 'sessions' object is missing", error_prefix.c_str());
    const Value &sessions = doc["sessions"];
    PPK_ASSERT_ERROR(sessions.IsArray(),
        "%s: the 'sessions' member is not an array", error_prefix.c_str());

    map<unsigned long, Session *> sessions_encountered
        = map<unsigned long, Session *>();

    /* A fake session representing the root of the dependency graph */
    Session *root_session = new Session(0, 0.0, 0);
    sessions_encountered.emplace(root_session->id, root_session);

    /* Create each session */
    for (SizeType i = 0; i < sessions.Size(); i++)
    {
        list<unsigned long> prec_sessions;
        list<unsigned long> tt_after_prec_sessions;
        Session *current_session = Parser::session_from_json_object(
            user_name, sessions[i], &prec_sessions, &tt_after_prec_sessions);

        PPK_ASSERT_ERROR(sessions_encountered.find(current_session->id)
                == sessions_encountered.end(),
            "%s: two sessions have the same id", error_prefix.c_str());
        sessions_encountered.emplace(current_session->id, current_session);

        /* Build the precedence relation between sessions */
        if (prec_sessions.size() == 0)
        {
            /* "fake" dependency to the root session, with weight the starting
             * time of current session */
            auto dependency = make_pair(
                root_session, current_session->original_first_submit_time);
            current_session->preceding_sessions.insert(dependency);
        }
        else
        {
            auto prec_it = prec_sessions.begin();
            auto tt_it = tt_after_prec_sessions.begin();
            while (prec_it != prec_sessions.end())
            {
                Session *prec_sess = sessions_encountered[*prec_it];
                auto dependency = make_pair(prec_sess, *tt_it);
                current_session->preceding_sessions.insert(dependency);

                prec_it++;
                tt_it++;
            }
        }
    }
    /* Go throught the sessions again to add pointers to following sessions */
    for (auto it : sessions_encountered)
    {
        Session *current_session = it.second;
        for (auto it2 : current_session->preceding_sessions)
        {
            Session *prec_sess = it2.first;
            prec_sess->following_sessions.insert(current_session);
        }
    }

    return root_session;
}

list<Profile *> Parser::profile_list_from_input_json(
    string user_name, const string input_json)
{
    Document doc = Parser::parse_input_json(input_json);
    string error_prefix = "Invalid JSON file '" + input_json + "'";

    PPK_ASSERT_ERROR(
        doc.IsObject(), "%s: not a JSON object", error_prefix.c_str());
    PPK_ASSERT_ERROR(doc.HasMember("profiles"),
        "%s: the 'profiles' object is missing", error_prefix.c_str());
    const Value &profiles = doc["profiles"];
    PPK_ASSERT_ERROR(profiles.IsObject(),
        "%s: the 'profiles' member is not an object", error_prefix.c_str());

    list<Profile *> profile_list;
    for (Value::ConstMemberIterator it = profiles.MemberBegin();
         it != profiles.MemberEnd(); ++it)
    {
        const Value &key = it->name;
        const Value &value = it->value;

        PPK_ASSERT_ERROR(key.IsString(),
            "%s: all children of the 'profiles' object must have a "
            "string key",
            error_prefix.c_str());
        string profile_name = key.GetString();
        PPK_ASSERT_ERROR(value.IsObject(),
            "%s: all children of the 'profiles' object must have a "
            "value that is a json object",
            error_prefix.c_str());

        Profile *profile = new Profile();
        profile->name = profile_name;
        profile->workload_name = user_name;

        StringBuffer buffer;
        Writer<StringBuffer> writer(buffer);
        value.Accept(writer);
        profile->description = buffer.GetString();

        profile_list.push_back(profile);
    }

    return profile_list;
}

Job *Parser::job_from_json_object(string user_name, const Value &object)
{
    PPK_ASSERT_ERROR(object.IsObject(), "Invalid json object: not an object");

    PPK_ASSERT_ERROR(
        object.HasMember("id"), "Invalid json object: no 'id' member");
    PPK_ASSERT_ERROR(object["id"].IsString() || object["id"].IsInt(),
        "Invalid json object: 'id' member should be a string or an int");
    PPK_ASSERT_ERROR(
        object.HasMember("res"), "Invalid json object: no 'res' member");
    PPK_ASSERT_ERROR(object["res"].IsInt(),
        "Invalid json object: 'res' member is not an integer");
    PPK_ASSERT_ERROR(object.HasMember("profile"),
        "Invalid json object: no 'profile' member");
    PPK_ASSERT_ERROR(object["profile"].IsString(),
        "Invalid json object: 'profile' member is not a string");
    PPK_ASSERT_ERROR(object.HasMember("subtime"),
        "Invalid json object: no 'subtime' member");
    PPK_ASSERT_ERROR(object["subtime"].IsNumber(),
        "Invalid json object: 'subtime' member is not a number");

    Job *j = new Job;
    j->id = object["id"].IsString() ? object["id"].GetString()
                                    : to_string(object["id"].GetInt());
    j->id = user_name + "!" + j->id;
    j->submission_time = object["subtime"].GetDouble();
    j->walltime = -1;
    j->has_walltime = true;
    j->nb_requested_resources = object["res"].GetInt();
    j->profile = object["profile"].GetString();

    if (object.HasMember("walltime"))
    {
        PPK_ASSERT_ERROR(object["walltime"].IsNumber(),
            "Invalid json object: 'walltime' member is not a number");
        j->walltime = object["walltime"].GetDouble();
    }

    PPK_ASSERT_ERROR(j->walltime == -1 || j->walltime > 0,
        "Invalid json object: 'walltime' should either be -1 (no walltime) "
        "or strictly positive.");

    if (j->walltime == -1)
        j->has_walltime = false;

    return j;
}

string Parser::json_description_string_from_job(const Job *job)
{
    PPK_ASSERT_ERROR(job->id != "", "A job must have the field 'id'.");
    PPK_ASSERT_ERROR(
        job->nb_requested_resources != 0, "A job must have the field 'res'.");
    PPK_ASSERT_ERROR(
        job->profile != "", "A job must have the field 'profile'.");

    if (job->has_walltime)
    {
        return str(
            boost::format("{\"id\": \"%s\", \"profile\": \"%s\", \"res\": %d, "
                          "\"subtime\": %f, \"walltime\": %f}")
            % job->id % job->profile % job->nb_requested_resources
            % job->submission_time % job->walltime);
    }
    else
    {
        return str(boost::format("{\"id\": \"%s\", \"profile\": \"%s\", "
                                 "\"res\": %d, \"subtime\": %f}")
            % job->id % job->profile % job->nb_requested_resources
            % job->submission_time);
    }
}

static void check_format_errors_session_json(const Value &object)
{
    /* id and first_sumbit_time filds */
    PPK_ASSERT_ERROR(object.IsObject(),
        "Invalid SABjson file: 'sessions' is not a json object");
    PPK_ASSERT_ERROR(object.HasMember("id"),
        "Invalid SABjson file: a session has no 'id' member");
    PPK_ASSERT_ERROR(object["id"].IsInt(),
        "Invalid SABjson file: session 'id' member should be an int");
    unsigned int id = object["id"].GetInt();
    PPK_ASSERT_ERROR(object.HasMember("first_submit_time"),
        "Invalid SABjson file: session %d has no 'first_submit_time' member",
        id);
    PPK_ASSERT_ERROR(object["first_submit_time"].IsNumber(),
        "Invalid SABjson file: session %d 'first_submit_time' member should be "
        "a number",
        id);

    /* arrays preceding_sessions and thinking_time_after_preceding_session */
    PPK_ASSERT_ERROR(object.HasMember("preceding_sessions"),
        "Invalid SABjson file: session %d has no 'preceding_sessions' member",
        id);
    PPK_ASSERT_ERROR(object.HasMember("thinking_time_after_preceding_session"),
        "Invalid SABjson file: session %d has no "
        "'thinking_time_after_preceding_session' member",
        id);
    const Value &prec_sess = object["preceding_sessions"];
    const Value &tt_after = object["thinking_time_after_preceding_session"];
    PPK_ASSERT_ERROR(prec_sess.IsArray(),
        "Invalid SABjson file: session %d 'preceding_sessions' member should "
        "be an array",
        id);
    PPK_ASSERT_ERROR(tt_after.IsArray(),
        "Invalid SABjson file: session %d "
        "'thinking_time_after_preceding_session' member should be an array",
        id);
    PPK_ASSERT_ERROR(prec_sess.Size() == tt_after.Size(),
        "Invalid SABjson file: session %d arrays 'preceding_sessions' and "
        "'thinking_time_after_preceding_session' should have the same size",
        id);
    for (SizeType i = 0; i < prec_sess.Size(); i++)
    {
        PPK_ASSERT_ERROR(prec_sess[i].IsInt(),
            "Invalid SABjson file: preceding sessions in session %d should be "
            "integers",
            id);
        PPK_ASSERT_ERROR(tt_after[i].IsNumber(),
            "Invalid SABjson file: thinking_time_after_preceding_session in "
            "session %d should be numbers",
            id);
    }

    /* jobs */
    PPK_ASSERT_ERROR(object.HasMember("nb_jobs"),
        "Invalid SABjson file: session %d has no 'nb_jobs' member", id);
    PPK_ASSERT_ERROR(object["nb_jobs"].IsInt(),
        "Invalid SABjson file: session %d 'nb_jobs' member should be an int",
        id);
    PPK_ASSERT_ERROR(object.HasMember("jobs"),
        "Invalid SABjson file: session %d has no 'jobs' member", id);
    const Value &jobs = object["jobs"];
    PPK_ASSERT_ERROR(jobs.IsArray(),
        "Invalid SABjson file: session %d 'jobs' member should be an array",
        id);
    PPK_ASSERT_ERROR(object["nb_jobs"].GetInt() > 0,
        "Invalid SABjson file: session %d should have at least one job", id);
    PPK_ASSERT_ERROR((int)jobs.Size() == object["nb_jobs"].GetInt(),
        "Invalid SABjson file: session %d field 'nb_jobs' should match the "
        "size of the jobs array",
        id);
}

Session *Parser::session_from_json_object(string user_name, const Value &object,
    list<unsigned long> *prec_sessions,
    list<unsigned long> *tt_after_prec_sessions)
{
    check_format_errors_session_json(object);

    const Value &prec_sess = object["preceding_sessions"];
    const Value &tt_after = object["thinking_time_after_preceding_session"];
    const Value &jobs = object["jobs"];

    Session *s = new Session(object["id"].GetUint(),
        lround(object["first_submit_time"].GetDouble()),
        object["nb_jobs"].GetUint());

    *prec_sessions = list<unsigned long>();
    *tt_after_prec_sessions = list<unsigned long>();
    for (SizeType i = 0; i < prec_sess.Size(); i++)
    {
        prec_sessions->push_back(prec_sess[i].GetUint());
        tt_after_prec_sessions->push_back(lround(tt_after[i].GetDouble()));
    }

    Job *j = Parser::job_from_json_object(user_name, jobs[0]);
    double sub_time = j->submission_time;
    PPK_ASSERT_ERROR(sub_time == 0,
        "Invalid SABjson file in session %lu: the first job should have a "
        "submission time of 0",
        s->id);

    for (SizeType i = 0; i < jobs.Size(); i++)
    {
        j = Parser::job_from_json_object(user_name, jobs[i]);
        PPK_ASSERT_ERROR(j->submission_time >= sub_time,
            "Invalid SABjson file in session %lu: 'jobs' should be ordered "
            "inside a session",
            s->id);
        sub_time = j->submission_time;
        /* Add the session id as a suffix of the job id, for log readability */
        j->id = j->id + ":s" + to_string(s->id);
        j->session = s;
        j->submission_time = lround(
            j->submission_time); /* round the job submission time to the nearest
                                    second, as more precision doesn't make sense
                                    in a user submission model (the "submit
                                    time" fields in the original SWF files are
                                    almost always intergers, anyway) */
        s->jobs.push_back(j);
    }

    return s;
}

void Parser::profile_from_duration(
    Profile *profile, const string duration, const string user_name)
{
    // TODO retrieve that info from the platform (wait batsim5.0)
    double platform_computing_speed = PLATFORM_COMPUTING_SPEED;

    profile->workload_name = user_name;
    profile->name = duration;
    double duration_time = stod(duration);
    profile->description = "{\"com\": 0.0, \"cpu\": "
        + to_string(duration_time * platform_computing_speed)
        + ", \"type\": \"parallel_homogeneous\"}";
}