#pragma once

#include <list>

#include "json_workload.hpp"

#include "schedule.hpp"

struct SortableJob
{
    const Job * job;
    Rational release_date;
};

class SortableJobOrder
{
public:
    struct CompareInformation
    {
        virtual ~CompareInformation() = 0;
    };

    struct UpdateInformation
    {
        UpdateInformation(Rational current_date);
        virtual ~UpdateInformation();

        Rational current_date;
    };

public:
    virtual ~SortableJobOrder();
    virtual bool compare(const SortableJob * j1, const SortableJob * j2, const CompareInformation * info = nullptr) const = 0;
    virtual void updateJob(SortableJob * job, const UpdateInformation * info = nullptr) const = 0;
    bool update_job_impact = true;

protected:
    JobComparator jobcmp;
};

class FCFSOrder : public SortableJobOrder
{
public:
    FCFSOrder();
    ~FCFSOrder();
    bool compare(const SortableJob * j1, const SortableJob * j2, const CompareInformation * info = nullptr) const;
    void updateJob(SortableJob * job, const UpdateInformation * info = nullptr) const;
};

class LCFSOrder : public SortableJobOrder
{
public:
    LCFSOrder();
    ~LCFSOrder();
    bool compare(const SortableJob * j1, const SortableJob * j2, const CompareInformation * info = nullptr) const;
    void updateJob(SortableJob * job, const UpdateInformation * info = nullptr) const;
};

class AscendingSizeOrder : public SortableJobOrder
{
public:
    AscendingSizeOrder();
    ~AscendingSizeOrder();
    bool compare(const SortableJob * j1, const SortableJob * j2, const CompareInformation * info = nullptr) const;
    void updateJob(SortableJob * job, const UpdateInformation * info = nullptr) const;
};

class DescendingSizeOrder : public SortableJobOrder
{
public:
    DescendingSizeOrder();
    ~DescendingSizeOrder();
    bool compare(const SortableJob * j1, const SortableJob * j2, const CompareInformation * info = nullptr) const;
    void updateJob(SortableJob * job, const UpdateInformation * info = nullptr) const;
};

class AscendingWalltimeOrder : public SortableJobOrder
{
public:
    AscendingWalltimeOrder();
    ~AscendingWalltimeOrder();
    bool compare(const SortableJob * j1, const SortableJob * j2, const CompareInformation * info = nullptr) const;
    void updateJob(SortableJob * job, const UpdateInformation * info = nullptr) const;

};

class DescendingWalltimeOrder : public SortableJobOrder
{
public:
    DescendingWalltimeOrder();
    ~DescendingWalltimeOrder();
    bool compare(const SortableJob * j1, const SortableJob * j2, const CompareInformation * info = nullptr) const;
    void updateJob(SortableJob * job, const UpdateInformation * info = nullptr) const;
};

class Queue
{
public:
    Queue(SortableJobOrder * order);
    ~Queue();

    /* Insert the job at the beginning of the queue, setting its release_date
     * to the current date. */
    void append_job(const Job * job, SortableJobOrder::UpdateInformation * update_info);

    /* Insert the job at the beginning of the queue, setting its release_date
     * to the job's submission date. */
    void insert_job(const Job * job);

    void insert_sorted_job(const Job * job,const SortableJobOrder::UpdateInformation * update_info ,
        const SortableJobOrder::CompareInformation * compare_info);
    std::list<SortableJob *>::iterator remove_job(const Job * job);
    std::list<SortableJob *>::iterator remove_job(std::list<SortableJob *>::iterator job_it);
    void sort_queue(SortableJobOrder::UpdateInformation * update_info, SortableJobOrder::CompareInformation * compare_info = nullptr);

    const Job * first_job() const;
    const Job * first_job_or_nullptr() const;
    bool contains_job(const Job * job) const;

    bool is_empty() const;
    int nb_jobs() const;

    std::string to_string() const;

    std::list<SortableJob *>::iterator begin();
    std::list<SortableJob *>::iterator end();

    std::list<SortableJob *>::const_iterator begin() const;
    std::list<SortableJob *>::const_iterator end() const;

private:
    std::list<SortableJob *> _jobs;
    SortableJobOrder * _order;
};
