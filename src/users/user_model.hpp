/**
 * Users simulated by following a submission model.
 */
#include "users/user.hpp"

/**
 * @brief A user that adapts his interarrival rate to feedback.
 */
class DichoIntersubmitTimeUser : public User
{
public:
    DichoIntersubmitTimeUser(string name, long date_first_submit,
        long date_end_submit, long initial_delay);
    DichoIntersubmitTimeUser(string name, const rapidjson::Value &param);
    ~DichoIntersubmitTimeUser();
    long next_submission();
    void jobs_to_submit(double date, list<shared_ptr<Job>> &jobs,
        list<const Profile *> &profiles);

protected:
    list<const Profile *> job_profiles_used();

private:
    long delay_inf = 0;
    double delay_sup;
    double delay_between_sumbit;
    int job_id = 0;
    shared_ptr<Job> last_job_submitted;

    // TODO retrieve that info from the platform (wait batsim5.0)
    double platform_computing_speed = PLATFORM_COMPUTING_SPEED;
};

/**
 * @brief A user that adapts his submission load to feedback.
 * @details The user connects routinely, every delay_between_sumbit.
 * If all his jobs are finished, he submit twice as many.
 * Otherwise he submits half as many.
 */
class RoutineGreedyUser : public User
{
public:
    RoutineGreedyUser(string name, long date_first_submit, long date_end_submit,
        long delay_between_sumbit, int initial_nb_job);
    RoutineGreedyUser(string name, const rapidjson::Value &param);
    ~RoutineGreedyUser();
    long next_submission();
    void jobs_to_submit(double date, list<shared_ptr<Job>> &jobs,
        list<const Profile *> &profiles);

protected:
    list<const Profile *> job_profiles_used();

private:
    long delay_between_sumbit;
    int nb_jobs_to_submit;
    bool first_submit = true;

    int job_id = 0;
    list<shared_ptr<Job>> last_jobs_submitted;
    /* Return true if all the jobs previously submitted by the user finished
     * successfully. Also delete the finished jobs. */
    bool previous_jobs_succedded();

    // TODO retrieve that info from the platform (wait batsim5.0)
    double platform_computing_speed = PLATFORM_COMPUTING_SPEED;
};

/**
 * @brief A user that thinks after the terminaison of her previous job before
 * submitting a new one.
 * @details When the job previously submitted finishes, the user is awaken by
 * the broker. She waits for some time (the `think_time`) before
 * submitting the next one.
 */
class ThinkTimeUser : public User
{
public:
    ThinkTimeUser(string name, long date_first_submit,
        unsigned long date_end_submit, unsigned long think_time);
    ThinkTimeUser(string name, const rapidjson::Value &param);
    ~ThinkTimeUser();
    long next_submission();
    void jobs_to_submit(double date, list<shared_ptr<Job>> &jobs,
        list<const Profile *> &profiles);
    void wake_on_feedback(double date, list<shared_ptr<Job>> &ended_jobs);

protected:
    list<const Profile *> job_profiles_used();

private:
    unsigned long think_time;
    int job_id = 0;
    shared_ptr<Job> last_job_submitted;

    // TODO retrieve that info from the platform (wait batsim5.0)
    double platform_computing_speed = PLATFORM_COMPUTING_SPEED;
};