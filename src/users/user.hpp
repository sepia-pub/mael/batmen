#pragma once

#include "json_workload.hpp"
#include "rapidjson/document.h"
#include <list>
#include <string>
#include <memory>

using namespace std;

class User
{
public:
    virtual ~User();

    /**
     * @brief Returns the next date at which the user wants to submit something,
     * DATE_NEVER if she has finished to submit or DATE_UNKNOWN if she is
     * waiting for feedback.
     */
    long next_submission() const;

    /**
     * @brief Return the jobs to submit by the user at this date
     * @details This method updates the field date_of_next_sumbission and is to
     * be called only at dates specified by next_submission().
     *
     * @param[in] date The date at which the jobs have been submitted
     * @param[out] jobs The list of jobs to submit (can be empty), each
     * containing a field to check their status.
     * @param[out] profiles The list of profiles used by the jobs, if new.
     */
    virtual void jobs_to_submit(
        double date, list<shared_ptr<Job>> &jobs, list<const Profile *> &profiles)
        = 0;

    /* For demand response users only: */

    /* For users reacting to feedback: */
    virtual void wake_on_feedback(double date, list<shared_ptr<Job>> &ended_jobs);

public:
    string user_name;
    long dm_stat[14];

protected:
    long date_of_next_submission;
    long end_submit;
};

/**
 * Custom order for the user queue. It will be ordered by
 *  - increasing date of next submission
 *  - the -2.0 (user is waiting for feedback) being placed after
 *  - the 1.0 (user has finished to submit) being placed even after, at the very
 *    end of the queue
 */
struct CompareUsers
{
    bool operator()(User *left, User *right)
    {
        if (left->next_submission() == DATE_NEVER)
            return false;
        if (right->next_submission() == DATE_NEVER)
            return true;
        if (left->next_submission() == DATE_UNKNOWN)
            return false;
        if (right->next_submission() == DATE_UNKNOWN)
            return true;

        return left->next_submission() < right->next_submission();
    }
};
