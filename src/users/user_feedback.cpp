#include "users/user_feedback.hpp"
#include "../pempek_assert.hpp"
#include "json_workload.hpp"
#include "rapidjson/document.h"
#include <limits>
#include <string>

/* FeedbackUser */
void FeedbackUser::init_FeedbackUser(
    std::string name, const rapidjson::Value &param)
{
    /* Parse some info from the json */
    PPK_ASSERT_ERROR(param.HasMember("input_json"),
        "Invalid user_description file: wrong 'param' for user %s, there "
        "should be an 'input_json' field",
        name.c_str());
    PPK_ASSERT_ERROR(param["input_json"].IsString(),
        "Invalid user_description file: 'input_json' should be a string for "
        "user %s",
        name.c_str());

    user_name = name;
    input_json = param["input_json"].GetString();

    /* Open the input file and build the session graph */
    Session *start_session
        = Parser::session_graph_from_SABjson(user_name, input_json);

    if (start_session->following_sessions.empty())
        date_of_next_submission = DATE_NEVER;
    else
    {
        /* Call the function in charge of closing a session and updating the
         * free sessions, specific to the replay model: */
        close_session(0.0, start_session);

        Session *next_session = free_sessions.top();
        date_of_next_submission = next_session->start_time;
    }
}

FeedbackUser::~FeedbackUser()
{
}

void FeedbackUser::update_date_next_sub()
{
    /* Calculate the date of next submission.
     * next_submit_time = DATE_UNKNOWN || DATE_NEVER ||
            min(free_session->next, active_session->next) */

    if (active_sessions.empty())
    {
        if (!free_sessions.empty())
            date_of_next_submission = free_sessions.top()->start_time;
        else if (ongoing_job_counter > 0)
            date_of_next_submission = DATE_UNKNOWN;
        else
            date_of_next_submission = DATE_NEVER;
        return;
    }

    /* Case active_sessions not empty: the next submit time is known.
     * Calculate the min. */
    date_of_next_submission = std::numeric_limits<long>::max();
    for (Session *active_sess : active_sessions)
    {
        PPK_ASSERT_ERROR(!active_sess->jobs.empty(),
            "Active session %lu has no job to submit", active_sess->id);

        long next_call = lround(active_sess->start_time
            + active_sess->jobs.front()->submission_time);

        date_of_next_submission = min(date_of_next_submission, next_call);
    }

    if (!free_sessions.empty())
        date_of_next_submission
            = min(date_of_next_submission, free_sessions.top()->start_time);
    return;
}

void FeedbackUser::jobs_to_submit(double date, std::list<shared_ptr<Job>> &jobs,
    std::list<const Profile *> &profiles)
{
    jobs = std::list<shared_ptr<Job>>();
    profiles = std::list<const Profile *>();

    /* Add the free sessions starting now to the list of active sessions */
    while (!free_sessions.empty()
        && free_sessions.top()->start_time == lround(date))
    {
        active_sessions.push_back(free_sessions.top());
        free_sessions.pop();
    }
    PPK_ASSERT_ERROR(!active_sessions.empty(),
        "User %s has been called to sumbit but she has no active session."
        "Her next_submission date is %lu, her next free session start time is "
        "%lu",
        user_name.c_str(), this->date_of_next_submission,
        free_sessions.top()->start_time);

    /* For each active session, add the jobs to submit now to the list `jobs` */
    for (auto iter = active_sessions.begin(); iter != active_sessions.end();)
    {
        Session *active_sess = *iter;
        auto job_list = &active_sess->jobs;
        PPK_ASSERT_ERROR(!job_list->empty(),
            "Active session %lu has no job to submit", active_sess->id);

        long offset = active_sess->start_time;

        while (!job_list->empty()
            && job_list->front()->submission_time + offset == lround(date))
        {
            shared_ptr<Job> job = shared_ptr<Job>(new Job(
                *job_list->front())); // Cast const Job * -> shared_ptr<Job>
            Profile *job_profile = new Profile();

            /* Handle the job according to the user behavior. */
            bool execute_now = handle_job(date, job, job_profile);

            if (execute_now)
            {
                job->submission_time = date;
                /* Add it to the list of jobs (and profiles, if new) to submit
                 */
                jobs.push_back(job);
                if (sent_profiles.count(job->profile) == 0)
                {
                    profiles.push_back(job_profile);
                    sent_profiles.insert(job->profile);
                }
                ongoing_job_counter++;

                /* Delete job from the job list */
                job_list->pop_front();
            }
        }

        if (job_list->empty()) /* active session finished: close it */
            iter = active_sessions.erase(iter);
        else
            ++iter;
    }

    PPK_ASSERT_ERROR(!jobs.empty(),
        "User %s called to submit but did not submit any job",
        user_name.c_str());

    /* Update next submit time */
    update_date_next_sub();
}

void FeedbackUser::wake_on_feedback(
    double date, std::list<shared_ptr<Job>> &ended_jobs)
{
    /* Propagate the info and close finished sessions */
    for (shared_ptr<Job> j : ended_jobs)
    {
        j->session->unfinished_jobs--;
        PPK_ASSERT_ERROR(ongoing_job_counter > 0,
            "Job %s of user %s finished but she had no ongoing jobs",
            j->id.c_str(), user_name.c_str());
        ongoing_job_counter--;

        if (j->session->unfinished_jobs == 0)
            close_session(date, j->session);
    }

    /* The next submit time might have changed in reaction to feedback */
    update_date_next_sub();
}

/* FBUserThinkTimeOnly */
FBUserThinkTimeOnly::FBUserThinkTimeOnly(
    std::string name, const rapidjson::Value &param)
{
    init_FeedbackUser(name, param);
}

FBUserThinkTimeOnly::~FBUserThinkTimeOnly()
{
}

void FBUserThinkTimeOnly::close_session(double date, Session *finished_session)
{
    /* Remove the finished session from the dependancy list of its following
     * sessions, updating their projected start time.
     * If some sessions are made free that way, add them to the list of free
     * sessions. */
    for (Session *s : finished_session->following_sessions)
    {
        double tt_after_finished_session
            = s->preceding_sessions.at(finished_session);

        if (s->start_time == DATE_UNKNOWN)
            s->start_time = lround(date + tt_after_finished_session);
        else
            s->start_time = max(s->start_time,
                (long)lround(date + tt_after_finished_session));

        s->preceding_sessions.erase(finished_session);

        bool is_free_session = s->preceding_sessions.empty();
        if (is_free_session)
            free_sessions.push(s);
    }

    /* The finished session won't be used anymore: free memory */
    delete finished_session;
}

bool FBUserThinkTimeOnly::handle_job(
    double date, shared_ptr<Job> job, Profile *profile)
{
    Parser::profile_from_duration(
        profile, job->profile, user_name);
    return true;
}