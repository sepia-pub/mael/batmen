
#include "user_windows.hpp"
#include "../pempek_assert.hpp"
#include "users/response_behaviors.hpp"

DMWindow_list::DMWindow_list(const IntervalSet &interval)
{
    this->interval = interval;
}

bool DMWindow_list::date_in_dm_window(double date) const
{
    /* Returns true if the date is in one of the dm_window of the DMWindow_list
     */

    return interval.contains((int)date);
}

bool DMWindow_list::have_no_common_element(DMWindow_list *compare_list) const
{
    IntervalSet intersect = interval;
    intersect &= compare_list->interval;
    return intersect.is_empty();
}

DMWindowAutomata::DMWindowAutomata(DMWindow *dm_window,
    DMWindow_list *red_windows, DMWindow_list *yellow_windows)
{
    this->dm_window = dm_window;
    this->red_windows = red_windows;
    this->yellow_windows = yellow_windows;
}
bool DMWindowAutomata::is_in_red_state(double date)
{
    // Check whether the date is in a red window
    return (dm_window && dm_window->date_in_dm_window(date))
        || (red_windows && red_windows->date_in_dm_window(date));
}
bool DMWindowAutomata::is_in_yellow_state(double date)
{
    // Check whether the date is in yellow window
    return yellow_windows && yellow_windows->date_in_dm_window(date);
}
bool DMWindowAutomata::has_red_state()
{
    return dm_window || red_windows;
}
bool DMWindowAutomata::has_yellow_state()
{
    return yellow_windows;
}
DMWindowAutomata::~DMWindowAutomata()
{
}

StateAutomata::~StateAutomata()
{
}

DMUserMultiBehavior::DMUserMultiBehavior(const std::string &name,
    const rapidjson::Value &param, uint_fast32_t random_seed,
    StateAutomata *state_automata, LoggerUserStat *logger)
{
    init_ReplayUser(name, param);

    this->state_automata = state_automata;
    this->logger = logger;
    random_gen = std::mt19937(random_seed);
    red_prob_multi_core = vector<double>(R_TOTAL_MULTI, 0.0);
    red_prob_mono_core = vector<double>(R_TOTAL_MONO, 0.0);
    yellow_prob_mono_core = vector<double>(Y_TOTAL_MONO, 0.0);
    yellow_prob_multi_core = vector<double>(Y_TOTAL_MULTI, 0.0);
    check_deprecated_param(param);
    init_prob_mono_core(param, red_prob_mono_core, yellow_prob_mono_core);
    init_prob_multi_core(param, red_prob_multi_core, yellow_prob_multi_core);
}

double DMUserMultiBehavior::parse_proba_param(const rapidjson::Value &param,
    const std::vector<string> &config_param,
    std::vector<double> &probability_array, const std::string &window_name,
    const std::string &type_prob_name)
{
    // fill the config_param fields to probability_array
    double prob_total = 0.0;
    for (std::vector<double>::size_type i = 0; i < probability_array.size();
         i++)
    {
        std::string current_param = config_param[i];
        if (param.HasMember(current_param.c_str()))
        {
            PPK_ASSERT_ERROR(param[current_param.c_str()].IsDouble()
                    && param[current_param.c_str()].GetDouble() >= 0.0,
                "Error every specified probability should be a non-negative "
                "Double");
            probability_array[i] = param[current_param.c_str()].GetDouble();
            prob_total += probability_array[i];
        }
    }
    std::string error_message = "Error in parameter defined for user ";
    error_message += user_name;
    error_message
        += ". The sum of the probability given in parameter sum to 0.0 for ";
    error_message += window_name;
    error_message += ". Check that you gave";
    error_message += type_prob_name;
    error_message += " parameter to user and at least one non-zero probability";
    PPK_ASSERT_ERROR(prob_total != 0.0, "%s", error_message.c_str());
    return prob_total;
}
void DMUserMultiBehavior::init_prob_mono_core(const rapidjson::Value &param,
    std::vector<double> &red_prob_array, std::vector<double> &yellow_prob_array)
{
    // Red window probability initialization
    if (state_automata->has_red_state())
    {
        std::vector<string> red_config(R_TOTAL_MONO, "");
        red_config[R_DEGRAD_MONO] = "red_prob_degrad_mono_core";
        red_config[R_C_YOU_LATER_MONO] = "red_prob_see_you_later_mono_core";
        red_config[R_RENOUNCE_MONO] = "red_prob_renounce_mono_core";
        red_config[R_RIGID_MONO] = "red_prob_rigid_mono_core";
        red_prob_total_mono_core = parse_proba_param(param, red_config,
            red_prob_array, "red_windows", "red_windows_prob_mono_core");
    }

    // Yellow probability Initialization
    if (state_automata->has_yellow_state())
    {
        std::vector<string> yellow_config(Y_TOTAL_MONO, "");
        yellow_config[Y_DEGRAD_MONO] = "yellow_prob_degrad_mono_core";
        yellow_config[Y_RIGID_MONO] = "yellow_prob_rigid_mono_core";
        yellow_prob_total_mono_core
            = parse_proba_param(param, yellow_config, yellow_prob_array,
                "yellow_windows", "yellow_windows_prob_mono_core");
    }
}

void DMUserMultiBehavior::init_prob_multi_core(const rapidjson::Value &param,
    std::vector<double> &red_prob_array, std::vector<double> &yellow_prob_array)
{
    // Red window probability initialization
    if (state_automata->has_red_state())
    {
        std::vector<string> red_config(R_TOTAL_MULTI, "");
        red_config[R_DEGRAD_MULTI] = "red_prob_degrad_multi_core";
        red_config[R_C_YOU_LATER_MULTI] = "red_prob_see_you_later_multi_core";
        red_config[R_RECONFIG_MULTI] = "red_prob_reconfig_multi_core";
        red_config[R_RENOUNCE_MULTI] = "red_prob_renounce_multi_core";
        red_config[R_RIGID_MULTI] = "red_prob_rigid_multi_core";
        red_prob_total_multi_core = parse_proba_param(param, red_config,
            red_prob_array, "red_windows", "red_windows_prob_multi_core");
    }

    // Yellow probability Initialization
    if (state_automata->has_yellow_state())
    {
        std::vector<string> yellow_config(Y_TOTAL_MULTI, "");
        yellow_config[Y_DEGRAD_MULTI] = "yellow_prob_degrad_multi_core";
        yellow_config[Y_RECONFIG_MULTI] = "yellow_prob_reconfig_multi_core";
        yellow_config[Y_RIGID_MULTI] = "yellow_prob_rigid_multi_core";
        yellow_prob_total_multi_core
            = parse_proba_param(param, yellow_config, yellow_prob_array,
                "yellow_windows", "yellow_windows_prob_multi_core");
    }
}
void DMUserMultiBehavior::check_deprecated_param(const rapidjson::Value &param)
{
    std::vector<string> deprecated_option;
    deprecated_option.emplace_back("red_prob_degrad");
    deprecated_option.emplace_back("red_prob_see_you_later");
    deprecated_option.emplace_back("red_prob_renounce");
    deprecated_option.emplace_back("red_prob_reconfig");
    deprecated_option.emplace_back("red_prob_rigid");
    deprecated_option.emplace_back("yellow_prob_degrad");
    deprecated_option.emplace_back("yellow_prob_reconfig");
    deprecated_option.emplace_back("yellow_prob_rigid");
    for (const std::string &option : deprecated_option)
    {
        PPK_ASSERT_ERROR(!(param.HasMember(option.c_str())), "%s %s,%s",
            "Use of deprecated option for user,option :", user_name.c_str(),
            option.c_str());
    }
}
DMUserMultiBehavior::~DMUserMultiBehavior()
{
    if (original_trace)
    {
        delete original_trace;
        original_trace = nullptr;
    }
}

void DMUserMultiBehavior::jobs_to_submit(double date,
    std::list<shared_ptr<Job>> &jobs, std::list<const Profile *> &profiles)
{
    ReplayUser::jobs_to_submit(date, jobs, profiles);
}

bool DMUserMultiBehavior::exec_red_behavior_mono(
    std::vector<double>::size_type behavior_number, double date,
    shared_ptr<Job> job, Profile *profile, double random_number)
{
    if (behavior_number == R_RENOUNCE_MONO)
    {
        return renounce_job(job, this, random_number);
    }
    if (behavior_number == R_C_YOU_LATER_MONO)
    {
        return delay_job(
            date + 3600, job, original_trace, this, random_number, true);
    }
    if (behavior_number == R_DEGRAD_MONO)
    {
        log_behavior(job, "consider_degrad", 0, random_number);
        return degrad_temp_job(job, profile, this, random_number);
    }
    if (behavior_number == R_RIGID_MONO)
    {
        return rigid_job(job, profile, this, random_number);
    }

    PPK_ASSERT_ERROR(false, "Undefined behavior number...");
    return false;
}

bool DMUserMultiBehavior::red_window_behavior_mono_core(
    double date, shared_ptr<Job> job, Profile *profile)
{
    double behavior = distribution(random_gen) * red_prob_total_mono_core;
    double total_probability = 0.0;
    for (std::vector<double>::size_type i = 0; i < red_prob_mono_core.size();
         i++)
    {
        total_probability += red_prob_mono_core[i];
        if (behavior < total_probability)
        {
            return exec_red_behavior_mono(
                i, date, job, profile, behavior / red_prob_total_mono_core);
        }
    }
    /* if none of the above we launch the job without changing anything
    Rigid strategy*/
    return rigid_job(job, profile, this, behavior / red_prob_total_mono_core);
}

bool DMUserMultiBehavior::exec_red_behavior_multi(
    std::vector<double>::size_type behavior_number, double date,
    shared_ptr<Job> job, Profile *profile, double random_number)
{
    if (behavior_number == R_RENOUNCE_MULTI)
    {
        return renounce_job(job, this, random_number);
    }
    if (behavior_number == R_C_YOU_LATER_MULTI)
    {
        return delay_job(
            date + 3600, job, original_trace, this, random_number, true);
    }
    if (behavior_number == R_DEGRAD_MULTI)
    {
        log_behavior(job, "consider_degrad", 0, random_number);
        return degrad_space_job(job, profile, this, random_number);
    }
    if (behavior_number == R_RECONFIG_MULTI)
    {
        log_behavior(job, "consider_reconfig", 0, random_number);
        return reconfig_job(job, profile, 1.0, this, random_number);
    }
    if (behavior_number == R_RIGID_MULTI)
    {
        return rigid_job(job, profile, this, random_number);
    }
    else
    {
        PPK_ASSERT_ERROR(false, "Undefined behavior number...");
        return false;
    }
}

bool DMUserMultiBehavior::red_window_behavior_multi_core(
    double date, shared_ptr<Job> job, Profile *profile)
{
    double behavior = distribution(random_gen) * red_prob_total_multi_core;
    double total_probability = 0.0;
    for (std::vector<double>::size_type i = 0; i < red_prob_multi_core.size();
         i++)
    {
        total_probability += red_prob_multi_core[i];
        if (behavior < total_probability)
        {
            return exec_red_behavior_multi(
                i, date, job, profile, behavior / red_prob_total_multi_core);
        }
    }
    return rigid_job(job, profile, this, behavior / red_prob_total_multi_core);
}

bool DMUserMultiBehavior::red_window_behavior(
    double date, shared_ptr<Job> &job, Profile *profile)
{
    /*
     * We decide at random the behavior
     * (renounce, C_you_later, degrad, reconfig,rigid)
     * that will be done on this job
     */
    if (job->nb_requested_resources == 1)
    {
        return red_window_behavior_mono_core(date, job, profile);
    }
    else
    {
        return red_window_behavior_multi_core(date, job, profile);
    }
}
bool DMUserMultiBehavior::yellow_window_behavior_mono_core(
    shared_ptr<Job> job, Profile *profile)
{
    double behavior = distribution(random_gen) * yellow_prob_total_mono_core;
    double total_probability = 0.0;
    total_probability += yellow_prob_mono_core[Y_DEGRAD_MONO];
    if (behavior < total_probability)
    {
        log_behavior(
            job, "consider_degrad", 0, behavior / yellow_prob_total_mono_core);
        return degrad_temp_job(
            job, profile, this, behavior / yellow_prob_total_mono_core);
    }
    // if none of the above we launch the job without i.e. rigid strategy
    return rigid_job(
        job, profile, this, behavior / yellow_prob_total_mono_core);
}
bool DMUserMultiBehavior::yellow_window_behavior_multi_core(
    shared_ptr<Job> job, Profile *profile)
{
    double behavior = distribution(random_gen) * yellow_prob_total_multi_core;
    double total_probability = 0.0;
    total_probability += yellow_prob_multi_core[Y_RECONFIG_MULTI];
    if (behavior < total_probability)
    {
        log_behavior(job, "consider_reconfig", 0,
            behavior / yellow_prob_total_multi_core);
        return reconfig_job(
            job, profile, 1.0, this, behavior / yellow_prob_total_multi_core);
    }
    total_probability += yellow_prob_multi_core[Y_DEGRAD_MULTI];
    if (behavior < total_probability)
    {
        log_behavior(
            job, "consider_degrad", 0, behavior / yellow_prob_total_multi_core);
        return degrad_space_job(
            job, profile, this, behavior / yellow_prob_total_multi_core);
    }
    // if none of the above we launch the job without i.e. rigid strategy
    return rigid_job(
        job, profile, this, behavior / yellow_prob_total_multi_core);
}
bool DMUserMultiBehavior::yellow_window_behavior(
    shared_ptr<Job> &job, Profile *profile)
{
    /*
     * We decide at random the behavior (rigid, degrad, reconfig)
     * that will be done on this job
     */
    if (job->nb_requested_resources == 1)
    {
        return yellow_window_behavior_mono_core(job, profile);
    }
    else
    {
        return yellow_window_behavior_multi_core(job, profile);
    }
}

bool DMUserMultiBehavior::handle_job(
    double date, shared_ptr<Job> job, Profile *profile)
{
    // red_windows and dm_windows check
    if (state_automata->is_in_red_state(date))
    {
        return red_window_behavior(date, job, profile);
    }

    // yellow_windows check
    if (state_automata->is_in_yellow_state(date))
    {
        /*
         * We decide at random the behavior (rigid, degrad, reconfig)
         * that will be done on this job
         */
        return yellow_window_behavior(job, profile);
    }
    else
    {
        return rigid_job(job, profile, this, 1.0);
    }
}
