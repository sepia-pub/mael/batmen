#pragma once

#include "../isalgorithm.hpp"
#include "broker.hpp"
#include "json_workload.hpp"

/**
 * A scheduler inheriting from this class will handle dynamic jobs submitted
 * by the broker, if any.
 * To do so, it asks the broker for the next submission date and uses batsim
 * REQUESTED_CALL to come back to the broker when it has something to submit.
 */
class DynScheduler : public ISchedulingAlgorithm
{
public:
    DynScheduler(Workload *workload, SchedulingDecision *decision, Queue *queue,
        ResourceSelector *selector, double rjms_delay,
        rapidjson::Document *variant_options);

    virtual ~DynScheduler();

    /* Overide a bunch of functions to communicate with the broker
     * in reaction to several events. */

    /**
     * Checks if the broker mode is enabled
     * and initialize some variables if so.
     */
    virtual void on_simulation_start(
        double date, const rapidjson::Value &batsim_config);

    /**
     * @brief Will handle feedback to users. Must be called by sons of this
     * class AT THE END of their make_decision implementation.
     */
    virtual void make_decisions(double date,
        SortableJobOrder::UpdateInformation *update_info,
        SortableJobOrder::CompareInformation *compare_info);
    virtual void on_requested_call(double date);

protected:
    /**
     * To be called when they are no more dynamic jobs to submit. Acknowledge
     * that to batsim and print the user_stat_log if needed.
     */
    void end_broker(double date);

    /**
     * Asks the broker for its jobs to submit, and submit them.
     */
    void submit_broker_jobs(double date);

    /**
     * Dynamically registers to batsim the job `jobs` to submit.
     */
    void dyn_submit(std::list<Job> jobs, double date);

    /**
     * Registers to batsim the profiles that will be used by the broker.
     */
    void dyn_register_profiles(std::list<const Profile *> profiles, double date);

protected:
    bool broker_enabled = false;
    Broker *broker = nullptr;
    double date_of_next_submission;
    bool next_submission_has_changed = false;

private:
    std::string log_folder = "";
};