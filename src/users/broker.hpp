#include "../isalgorithm.hpp"
#include "user_feedback.hpp"
#include "user_model.hpp"
#include "user_replay.hpp"
#include "user_windows.hpp"
#include <list>
#include <map>
#include <set>
#include <string>
#include <vector>
using namespace std;

class Broker
{
public:
    Broker(rapidjson::Document *user_description_file);
    static DMWindow_list* parse_dm_windows(const std::string& attr_name,
        const rapidjson::Document  *user_description_file);
    ~Broker();

    /**
     * @brief Returns the date (in seconds, rounded to the nearest) of the next
     * submission by the broker.
     * @details Two numbers are used as flags:
     *    DATE_NEVER = -1 if the broker has finished to submit.
     *    DATE_UNKNOWN = -2 if the broker doesn't have a next submission date
     * yet (user waiting for feedback)
     */
    long next_submission(double date) const;

    /**
     * @brief Return the list of jobs to submit by any of the users.
     * @param[in] date The date at which the jobs have been submitted
     * @param[out] jobs The list of jobs to submit (can be empty), each
     * containing a field to check their status.
     * @param[out] profiles The list of profiles used by the jobs, if new.
     */
    void jobs_to_submit(
        double date, list<Job> &jobs, list<const Profile *> &profiles);

    /**
     * @brief Acknowledge the latest execution-related activity and forward the
     * info to interested users.
     */
    void feedback_job_status(double date, vector<string> &jobs_ended,
        vector<string> &jobs_killed, vector<string> &jobs_released);
    void log_user_stats(string user_stat_file);
    void update_status_if_dyn_job(const string &job_id, JobStatus status);


private:
    map<string, User *> users;
    unsigned int core_limit_per_user = std::numeric_limits<int>::max();
    list<User *> user_queue;
    map<string, shared_ptr<Job>> dynamic_jobs = map<string, shared_ptr<Job>>();
    map<User *, list<shared_ptr<Job>>> users_to_wake
        = map<User *, list<shared_ptr<Job>>>();
    LoggerUserStat *logger_user_stat = nullptr;
private:
    /* Deterministic generation of seeds for users that use randomness */
    mt19937 seed_generator = mt19937(1997);

    /* (Optional) The demand response window for the DM users */
    DMWindow *dm_window = nullptr;

    /* (Optional) Red and Yellow windows for MultiBehavior DM users */
    DMWindow_list* red_windows_list=nullptr;
    DMWindow_list* yellow_windows_list = nullptr;
    DMWindowAutomata* state_automata = nullptr;
};