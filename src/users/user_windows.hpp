
#pragma once
#include "users/user.hpp"
#include <random>
#include "queue.hpp"
#include "users/user_replay.hpp"
#include "users/log_user_stat.hpp"

/**
 * @brief list of date interval
 * @details DMWindow_list is a struct that contains a list of DMwindow and
 * provide function to check whether or not we are
 * in one of these windows. The interval in the list are assumed to be disjoint
 * (i.e the intersection between one interval and another is empty) and sorted
 * (i.e lexicographic order on the boundaries)
 */

struct DMWindow_list
{
    IntervalSet interval;
    explicit DMWindow_list(const IntervalSet& interval);
    bool have_no_common_element( DMWindow_list*   compare_list) const;

    bool date_in_dm_window(double date) const;
};


class StateAutomata {
public:
    virtual bool is_in_red_state(double date) = 0;
    virtual bool is_in_yellow_state(double date) =0;
    virtual bool has_red_state()=0;
    virtual bool has_yellow_state()=0;
    virtual ~StateAutomata();
};

class DMWindowAutomata: public StateAutomata {
public:
    DMWindowAutomata(DMWindow* dm_window,DMWindow_list* red_windows, DMWindow_list* yellow_windows);
    bool is_in_yellow_state(double date);
    bool is_in_red_state(double date);
    bool has_yellow_state();
    bool has_red_state();
    ~DMWindowAutomata();
private:

    DMWindow* dm_window;
    DMWindow_list* red_windows;
    DMWindow_list* yellow_windows;
};
/**
 * @brief User class that adopts a different set of submission behaviors depending
 * on the energy state (red, yellow, green)
 * @details Below are the behaviors drawn at random when the date is in a particular window
 * - red window: behaviors degrad, c_you_later, renounce, reconfig and rigid
 * - yellow window: behaviors degrad, reconfig and rigid
 * - otherwise ("green window"): rigid
 * See the documentation of red_window_behavior and yellow_window_behavior.
 */

class DMUserMultiBehavior :  public ReplayUser
{
public:
    DMUserMultiBehavior(
            const std::string & name, const rapidjson::Value &param,
            uint_fast32_t random_seed, StateAutomata* state_automata, LoggerUserStat* logger);
    ~DMUserMultiBehavior();
    double next_submission();
    void jobs_to_submit(
            double date, std::list<shared_ptr<Job> > &jobs, std::list< const Profile *> &profiles) override;


protected:
    enum  red_behavior_multi {R_RENOUNCE_MULTI,R_C_YOU_LATER_MULTI,R_DEGRAD_MULTI,
        R_RECONFIG_MULTI,R_RIGID_MULTI,R_TOTAL_MULTI};
    enum red_behavior_mono {R_RENOUNCE_MONO,R_C_YOU_LATER_MONO,R_DEGRAD_MONO,
        R_RIGID_MONO,R_TOTAL_MONO};
    enum  yellow_behavior_multi {Y_DEGRAD_MULTI,Y_RECONFIG_MULTI,Y_RIGID_MULTI,Y_TOTAL_MULTI};
    enum  yellow_behavior_mono {Y_DEGRAD_MONO,Y_RIGID_MONO,Y_TOTAL_MONO};
    StateAutomata* state_automata;
    std::mt19937 random_gen;
    std::uniform_real_distribution<double> distribution
            = std::uniform_real_distribution<double>(0.0, 1.0);

    double parse_proba_param(const rapidjson::Value & param,
        const std::vector<string> & config_param,std::vector<double> & probability_array,
        const std::string & window_name, const std::string & type_prob_name);
    void init_prob_mono_core(const rapidjson::Value &param,
        vector<double> &red_prob_array, vector<double> & yellow_prob_array);
    void init_prob_multi_core(const rapidjson::Value &param,
        vector<double> &red_prob_array, vector<double> & yellow_prob_array);
    void check_deprecated_param(const rapidjson::Value &param);

    /**
     *  @brief function called each time the user want to submit a job
     *  @details This function does the following :
     *          - if the user is in a red window
     *          the user adopt the behavior define in red_window_behavior
     *          - if the user is in a yellow window AND not in a red one
     *          the user adopt the behavior define in yellow_windows_behavior
     *          - else the user submit its job without any change
     */
    bool handle_job(double date,shared_ptr<Job> job,Profile *profile) override;

    /**
     *  @brief function called when the user submit a job in red state
     *  @details This function do the 5 behavior for red windows of the class
     *  (degrad,reconfig,renounce,rigid,see_you_later)
     *  following the provided probabilities given at the creation of the class
     */
    bool red_window_behavior(double date,shared_ptr<Job> & job, Profile *profile);
    bool red_window_behavior_mono_core(double date,shared_ptr<Job> job,Profile *profile);
    bool red_window_behavior_multi_core(double date,shared_ptr<Job> job, Profile *profile);
    bool exec_red_behavior_multi(unsigned long behavior_number, double date,
        shared_ptr<Job> job, Profile *profile,double random_number);
    bool exec_red_behavior_mono(std::vector<double>::size_type behavior_number,
        double date,shared_ptr<Job> job,Profile *profile,double random_number);
    /**
     *  @brief function called when the user submit a job in yellow state
     *  @details This function do the 3 behaviors for yellow windows (degrad,reconfig, rigid)
     *  following the provided probabilities given at the creation of the class
     */
    bool yellow_window_behavior(shared_ptr<Job> & job, Profile *profile);
    bool yellow_window_behavior_mono_core(shared_ptr<Job>job,Profile *profile);
    bool yellow_window_behavior_multi_core(shared_ptr<Job> job,Profile *profile);
    std::vector<double> red_prob_multi_core;
    std::vector<double> red_prob_mono_core;
    double red_prob_total_multi_core;
    double red_prob_total_mono_core;
    std::vector<double> yellow_prob_multi_core;
    std::vector<double> yellow_prob_mono_core;
    double yellow_prob_total_multi_core;
    double yellow_prob_total_mono_core;

};
