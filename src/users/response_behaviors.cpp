#include "response_behaviors.hpp"
#include "json_workload.hpp"
#include "users/user.hpp"

#include "../pempek_assert.hpp"

bool rigid_job(shared_ptr<Job> job, Profile *profile, ReplayUser *user,
    double random_number)
{
    Parser::profile_from_duration(profile, job->profile, user->user_name);

    /* Log... */
    user->log_behavior(job, "rigid", 0, random_number);
    user->dm_stat[2 * RIGID]++;
    user->dm_stat[2 * RIGID + 1]
        += job->nb_requested_resources * std::stol(job->profile);

    /* Signals that the job must be executed */
    return true;
}

bool renounce_job(shared_ptr<Job> job, ReplayUser *user, double random_number)
{
    /* Log... */
    user->log_behavior(job, "renounce", 0, random_number);
    user->dm_stat[2 * RENOUNCED]++;
    user->dm_stat[2 * RENOUNCED + 1]
        += job->nb_requested_resources * std::stol(job->profile);

    /* Signals that the job must not be executed */
    return false;
}

bool reconfig_job(shared_ptr<Job> job, Profile *profile, double alpha,
    ReplayUser *user, double random_number)
{
    int orig_nb_core = job->nb_requested_resources;

    PPK_ASSERT_ERROR(orig_nb_core > 1,
        "Behavior 'reconfig' undefined for job of size 1. Job_id: %s",
        job->id.c_str());

    /* Log... */
    user->dm_stat[2 * RECONF]++;
    user->dm_stat[2 * RECONF + 1] += orig_nb_core * std::stol(job->profile);
    user->log_behavior(job, "reconfig", 0.0, random_number);

    /* Reconfig: divide by two rounded up the nb or cores requested */
    int n = (orig_nb_core + 1) / 2;

    /* Speedup model:
     *   - T_orig: the original execution time (on orig_nb_core cores)
     *   - T_1: the execution time on one core
     *   - T_n: the execution time on n cores
     * We have: T_1 = n^alpha * T_n = orig_nb_core^alpha * T_orig
     *          => T_n = (orig_nb_core / n)^alpha * T_orig
     */
    long T_orig = std::stol(job->profile);
    long T_n;
    if (orig_nb_core%n != 0 || alpha != 1.0)
    {
         T_n =(long) (( pow((double)orig_nb_core / n, alpha) )* (double) T_orig);
    }
    else
    {
         T_n = orig_nb_core/n *T_orig;
    }
    Parser::profile_from_duration(
        profile, std::to_string(T_n), user->user_name); // parallel_homogeneous
    job->nb_requested_resources = n;
    job->profile = profile->name = 'r' + std::to_string(T_orig);

    return true;
}

bool degrad_space_job(shared_ptr<Job> &job, Profile *profile, ReplayUser *user,
    double random_number)
{
    int orig_nb_core = job->nb_requested_resources;

    PPK_ASSERT_ERROR(orig_nb_core > 1,
        "Behavior 'degrad_space' undefined for job of size 1. Job_id: %s",
        job->id.c_str());

    /* Log... */
    user->dm_stat[2 * DEGRADED]++;
    user->dm_stat[2 * DEGRADED + 1] += orig_nb_core * std::stol(job->profile);
    user->log_behavior(job, "degrad_space", 0, random_number);

    /* Spatial degradation: divide by two rounded up the nb or
     * cores requested, and keep the original duration */
    job->nb_requested_resources = (orig_nb_core + 1) / 2;
    Parser::profile_from_duration(profile, job->profile, user->user_name);
    job->profile = profile->name = 'd' + profile->name;

    return true;
}

bool degrad_temp_job(shared_ptr<Job> &job, Profile *profile, ReplayUser *user,
    double random_number)
{
    long original_time = std::stol(job->profile);

    /* Log... */
    user->log_behavior(job, "degrad_time", 0, random_number);
    user->dm_stat[2 * DEGRADED]++;
    user->dm_stat[2 * DEGRADED + 1]
        += (job->nb_requested_resources) * original_time;

    /* time degrad: divide by two the execution time */
    long new_time = original_time / 2;
    Parser::profile_from_duration(
        profile, std::to_string(new_time), user->user_name);
    job->profile = profile->name = 't' + std::to_string(original_time);
    return true;
}

bool delay_job(double new_time, shared_ptr<Job> job,
    UserSubmitQueue *original_trace, ReplayUser *user, double random_number, bool log_as_c_you_later)
{
    /* Log... */
    string log_string = log_as_c_you_later ? "C_you_later" : "delay";
    user->log_behavior(
        job, log_string, new_time - job->submission_time, random_number);
    user->dm_stat[2 * DELAYED]++;
    user->dm_stat[2 * DELAYED + 1]
        += job->nb_requested_resources * std::stol(job->profile);

    /* Update submission date and put job back in the trace */
    job->submission_time = new_time;
    original_trace->insert_sorted(job.get());

    /* Return "Do not execute now" */
    return false;
}