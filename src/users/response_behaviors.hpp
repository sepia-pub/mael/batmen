#include "json_workload.hpp"
#include "queue.hpp"
#include "user_replay.hpp"

using namespace std;

enum Behavior
{
    RIGID,
    RENOUNCED,
    DELAYED,
    DEGRADED,
    RECONF,
    CONSIDER_DEGRADED,
    CONSIDER_RECONF
};

/**
 * @brief
 */
bool rigid_job(shared_ptr<Job> job, Profile *profile, ReplayUser *user,
    double random_number);

/**
 * @brief Renouncing consists in simply not submitting the job (and never submit
 * it in the future).
 */
bool renounce_job(shared_ptr<Job> job, ReplayUser *user, double random_number);

/**
 * @brief The spatial degradation is only available for jobs of size > 1. It
 * consists in dividing the number of cores requested by two (rounded up) while
 * keeping the original submission time.
 */
bool degrad_space_job(shared_ptr<Job> &job, Profile *profile, ReplayUser *user,
    double random_number);

/**
 * @brief The temporal degradation consists in dividing the execution time
 * by two.
 */
bool degrad_temp_job(shared_ptr<Job> &job, Profile *profile, ReplayUser *user,
    double random_number);

/**
 * @brief The reconfiguration is only available for jobs of size > 1. It
 * consists in dividing the number of cores requested by two (rounded up) and
 * scaling the execution time accordingly, following the speedup model.
 * @details Speedup model: T_1 = n^alpha * T_n, with
 *   - T_1: the execution time on 1 core
 *   - T_n: the execution time on n cores
 */
bool reconfig_job(shared_ptr<Job> job, Profile *profile, double alpha,
    ReplayUser *user, double random_number);

/**
 * @brief Delaying consists in not submitting the job straight away but at a
 * later date new_time.
 */
bool delay_job(double new_time, shared_ptr<Job> job,
    UserSubmitQueue *original_trace, ReplayUser *user, double random_number, bool log_as_c_you_later);