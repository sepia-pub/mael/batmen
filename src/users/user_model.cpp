#include "users/user_model.hpp"
#include <limits>

#include "../pempek_assert.hpp"
#include "json_workload.hpp"

/* DichoIntersubmitTimeUser */
DichoIntersubmitTimeUser::DichoIntersubmitTimeUser(std::string name,
    long first_submit, long date_end_submit, long initial_delay)
{
    user_name = name;
    date_of_next_submission = first_submit;
    end_submit = date_end_submit;
    delay_sup = initial_delay;
}

DichoIntersubmitTimeUser::DichoIntersubmitTimeUser(
    std::string name, const rapidjson::Value &param)
{
    PPK_ASSERT_ERROR(param.HasMember("first_sumbit")
            && param.HasMember("end_submit")
            && param.HasMember("initial_delay"),
        "Invalid user_description file: wrong 'param' for user %s",
        name.c_str());
    PPK_ASSERT_ERROR(param["first_sumbit"].IsUint()
            && param["end_submit"].IsUint()
            && param["initial_delay"].IsUint(),
        "Invalid user_description file: wrong type in 'param' for user %s",
        name.c_str());

    user_name = name;
    date_of_next_submission = param["first_sumbit"].GetUint();
    end_submit = param["end_submit"].GetUint();
    delay_sup = param["initial_delay"].GetUint();
}

DichoIntersubmitTimeUser::~DichoIntersubmitTimeUser()
{
}

void DichoIntersubmitTimeUser::jobs_to_submit(
    double date, std::list<shared_ptr<Job>> &jobs, std::list<const Profile *> &profiles)
{
    jobs = std::list<shared_ptr<Job>>();

    /* The last job is still running */
    if (last_job_submitted 
        && (last_job_submitted->status == WAITING
            || last_job_submitted->status == RUNNING))
    {
        delay_inf = std::floor(date - last_job_submitted->submission_time);
        // let's wait a big longer
        delay_between_sumbit = std::max(1.0, std::ceil(delay_sup / 2));
    }
    else
    {
        /* First job submitted by this user */
        if (!last_job_submitted)
        {
            delay_between_sumbit = delay_sup; // initial delay
            profiles = job_profiles_used();
        }
        /* The last job finished */
        else
        {
            delay_sup = std::ceil(date - last_job_submitted->submission_time);
            delay_between_sumbit = (delay_sup + delay_inf) / 2;
            profiles = std::list<const Profile *>(); // already submitted before
        }

        shared_ptr<Job>job = shared_ptr<Job>(new Job());
        job->id = user_name + "!" + std::to_string(job_id++);
        job->profile = "100_sec";
        job->submission_time = date;
        job->nb_requested_resources = 1;
        job->has_walltime = true;
        job->walltime = 3600;

        last_job_submitted = job;
        jobs.push_back(job);
    }

    /* Update the date of next submission */
    date_of_next_submission = lround(date + delay_between_sumbit);

    /* Stop condition */
    if (date_of_next_submission > end_submit)
        date_of_next_submission = DATE_NEVER;
}

std::list<const Profile *> DichoIntersubmitTimeUser::job_profiles_used()
{
    std::list<const Profile *> profiles;

    Profile *profile0 = new Profile();
    profile0->workload_name = user_name;
    profile0->name = "100_sec";
    profile0->description = "{\"com\": 0.0, \"cpu\": "
        + std::to_string(100 * platform_computing_speed)
        + ", \"type\": \"parallel_homogeneous\"}";

    profiles.push_back(profile0);

    return profiles;
}

/* RoutineGreedyUser */
RoutineGreedyUser::RoutineGreedyUser(std::string name, long date_first_submit,
    long date_end_submit, long delay_between_sumbit, int initial_nb_job)
{
    user_name = name;
    date_of_next_submission = date_first_submit;
    end_submit = date_end_submit;
    this->delay_between_sumbit = delay_between_sumbit;
    nb_jobs_to_submit = initial_nb_job;
}

RoutineGreedyUser::RoutineGreedyUser(
    std::string name, const rapidjson::Value &param)
{
    PPK_ASSERT_ERROR(param.HasMember("first_sumbit")
            && param.HasMember("end_submit")
            && param.HasMember("delay_between_sumbit")
            && param.HasMember("initial_nb_job"),
        "Invalid user_description file: wrong 'param' for user %s",
        name.c_str());
    PPK_ASSERT_ERROR(param["first_sumbit"].IsUint()
            && param["end_submit"].IsUint()
            && param["delay_between_sumbit"].IsUint()
            && param["initial_nb_job"].IsInt(),
        "Invalid user_description file: wrong type in 'param' for user %s",
        name.c_str());

    user_name = name;
    date_of_next_submission = param["first_sumbit"].GetUint();
    end_submit = param["end_submit"].GetUint();
    this->delay_between_sumbit = param["delay_between_sumbit"].GetUint();
    nb_jobs_to_submit = param["initial_nb_job"].GetInt();
}

RoutineGreedyUser::~RoutineGreedyUser()
{
}

bool RoutineGreedyUser::previous_jobs_succedded()
{
    bool res = true;
    for (auto iter = last_jobs_submitted.begin(); 
        iter != last_jobs_submitted.end();)
    {
        shared_ptr<Job> job = *iter;

        switch (job->status) {
            case WAITING: 
            case RUNNING:
                res = false;
                iter++;
                break;
            case KILLED:
                res = false;
                iter = last_jobs_submitted.erase(iter);
                break;
            case FINISHED:
                iter = last_jobs_submitted.erase(iter);
        }
    }
    return res;
}

void RoutineGreedyUser::jobs_to_submit(
    double date, std::list<shared_ptr<Job>> &jobs, std::list<const Profile *> &profiles)
{
    jobs = std::list<shared_ptr<Job>>();
    profiles = std::list<const Profile *>();

    /* First call: just submit the initial amount of jobs */
    if (first_submit)
    {
        first_submit = false;
        profiles = job_profiles_used();
    }

    /* All job finished: submit twice as many jobs */
    else if (previous_jobs_succedded())
        nb_jobs_to_submit
            = (nb_jobs_to_submit == 0) ? 1 : nb_jobs_to_submit * 2;

    /* Otherwise sumbit half as many jobs (rounded down) */
    else
        nb_jobs_to_submit /= 2;

    for (int i = 0; i < nb_jobs_to_submit; i++)
    {
        shared_ptr<Job> job = shared_ptr<Job>(new Job());
        job->id = user_name + "!" + std::to_string(job_id++);
        job->profile = "100_sec";
        job->submission_time = date;
        job->nb_requested_resources = 1;
        job->has_walltime = true;
        job->walltime = 3600;

        last_jobs_submitted.push_back(job);
        jobs.push_back(job);
    }

    /* Update the date of next submission */
    date_of_next_submission = lround(date + delay_between_sumbit);

    /* Stop condition */
    if (date_of_next_submission > end_submit)
        date_of_next_submission = DATE_NEVER;
}

std::list<const Profile *> RoutineGreedyUser::job_profiles_used()
{
    std::list<const Profile *> profiles;

    Profile *profile0 = new Profile();
    profile0->workload_name = user_name;
    profile0->name = "100_sec";
    profile0->description = "{\"com\": 0.0, \"cpu\": "
        + std::to_string(100 * platform_computing_speed)
        + ", \"type\": \"parallel_homogeneous\"}";

    profiles.push_back(profile0);

    return profiles;
}

/* ThinkTimeUser */
ThinkTimeUser::ThinkTimeUser(std::string name, long first_submit,
    unsigned long date_end_submit, unsigned long think_time)
{
    user_name = name;
    date_of_next_submission = first_submit;
    end_submit = date_end_submit;
    this->think_time = think_time;
}

ThinkTimeUser::ThinkTimeUser(std::string name, const rapidjson::Value &param)
{
    PPK_ASSERT_ERROR(param.HasMember("first_sumbit")
            && param.HasMember("end_submit") && param.HasMember("think_time"),
        "Invalid user_description file: wrong 'param' for user %s",
        name.c_str());
    PPK_ASSERT_ERROR(param["first_sumbit"].IsUint()
            && param["end_submit"].IsUint() && param["think_time"].IsUint(),
        "Invalid user_description file: wrong type in 'param' for user %s",
        name.c_str());

    user_name = name;
    date_of_next_submission = param["first_sumbit"].GetUint();
    end_submit = param["end_submit"].GetUint();
    think_time = param["think_time"].GetUint();
}

ThinkTimeUser::~ThinkTimeUser()
{
}

void ThinkTimeUser::jobs_to_submit(
    double date, std::list<shared_ptr<Job>> &jobs, std::list<const Profile *> &profiles)
{
    jobs = std::list<shared_ptr<Job>>();

    /* Stop condition reached */
    if (date > end_submit)
    {
        date_of_next_submission = DATE_NEVER;
    }
    else
    {
        /* First job submitted by this user */
        if (!last_job_submitted)
            profiles = job_profiles_used();
        else{
            PPK_ASSERT_ERROR(last_job_submitted->status == FINISHED
                || last_job_submitted->status == KILLED,
                "ThinkTimeUser %s called to submit, but her last job did not finish", user_name.c_str());
        }

        shared_ptr<Job> job = shared_ptr<Job>(new Job());
        job->id = user_name + "!" + std::to_string(job_id++);
        job->profile = "100_sec";
        job->submission_time = date;
        job->nb_requested_resources = 1;
        job->has_walltime = false;

        last_job_submitted = job;
        jobs.push_back(job);

        /* Date of next submission unknown for now... */
        date_of_next_submission = DATE_UNKNOWN;
    }
}

std::list<const Profile *> ThinkTimeUser::job_profiles_used()
{
    std::list<const Profile *> profiles;

    Profile *profile0 = new Profile();
    profile0->workload_name = user_name;
    profile0->name = "100_sec";
    profile0->description = "{\"com\": 0.0, \"cpu\": "
        + std::to_string(100 * platform_computing_speed)
        + ", \"type\": \"parallel_homogeneous\"}";

    profiles.push_back(profile0);

    return profiles;
}

void ThinkTimeUser::wake_on_feedback(double date, std::list<shared_ptr<Job>> &ended_jobs)
{
    /* The user has been awakened: her last job terminated */
    PPK_ASSERT_ERROR(last_job_submitted->status == KILLED
            || last_job_submitted->status == FINISHED,
        "User %s awakened but her last job is neither killed nor finished.",
        user_name.c_str());
    date_of_next_submission = lround(date + think_time);
}
