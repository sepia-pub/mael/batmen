#include "users/log_user_stat.hpp"

#include <utility>
#include "../pempek_assert.hpp"
BehaviorStat::BehaviorStat(
    shared_ptr<Job> job, std::string behavior_name, long time_delayed,double random_value){
    this->job = std::move(job);
    this->behavior_name = std::move(behavior_name);
    this->time_delayed = time_delayed;
    this->random_value = random_value;
}

std::vector<std::string> BehaviorStat::split_id(){
    std::string job_id = job->id;
    std::vector<std::string> splitting;
    splitting.emplace_back("");

    for (char i : job_id){
        if (i == '!'){
            splitting.emplace_back("");
        }
        else{
            splitting.back() += i;
        }
    }

    return splitting;
}
boost::basic_format<char> BehaviorStat::format(){
    std::vector<std::string> splitting = split_id();
    PPK_ASSERT_ERROR(splitting.size() >= 2,
        "Error logging the job behavior the job_id should be of the format"
        " user_id!job_id");
    return boost::format("%1%,%2%,%3%,%4%,%5%,%6%\n") % splitting[0] % splitting[1]
        % job->submission_time % behavior_name % time_delayed % random_value;
}

LoggerUserStat::LoggerUserStat(std::string log_folder){
    this->log_folder = std::move(log_folder);
    put_header = true;
    write_threshold = 4096;
}

void LoggerUserStat::add_stat(
      const shared_ptr<Job> & job,  std::string  behavior_name, const long time_delayed,const double random_value){
    //make a copy of the job info because it can change
    shared_ptr<Job> copy_job_ptr = std::make_shared<Job>(*job);
    BehaviorStat to_add = BehaviorStat(copy_job_ptr, std::move(behavior_name), time_delayed,random_value);
    behaviors.push_back(to_add);
    if (behaviors.size() >= write_threshold){
        log_stat();
    }
}

void LoggerUserStat::log_stat(){
    /**
     * This functions write log in the csv format the behaviors recorded in
     * the vector behaviors
     */

    std::ofstream file;


    if (put_header){
        /* first time we log we add the header and erase previous log */
        file.open(log_folder + "/user_stats_behaviors.csv");
        file << "user,job_id,submission_time,behavior_name,time_delayed,random_value_normalized\n";
        put_header = false;
    }
    else{
        /* after the first time we log we append the data to the file */
        file.open(log_folder + "/user_stats_behaviors.csv",
            std::ofstream::out | std::ofstream::app);
    }

    /* We write the content in the vector to the disk and then free the vector */
    for (auto read_behavior : behaviors){
        file << read_behavior.format();
    }
    behaviors.clear();
    file.close();
}