#include "users/user_replay.hpp"
#include "users/response_behaviors.hpp"
#include "../pempek_assert.hpp"
#include "json_workload.hpp"

/******************************************************************************
 ******************************* Replay users *********************************
 *****************************************************************************/
/* Master class ReplayUser */
void ReplayUser::init_ReplayUser(
    std::string name, const rapidjson::Value &param)
{
    /* Parse some info from the json */
    PPK_ASSERT_ERROR(param.HasMember("input_json"),
        "Invalid user_description file: wrong 'param' for user %s, there "
        "should be an 'input_json' field",
        name.c_str());
    PPK_ASSERT_ERROR(param["input_json"].IsString(),
        "Invalid user_description file: 'input_json' should be a string for "
        "user %s",
        name.c_str());

    user_name = name;
    input_json = param["input_json"].GetString();

    /* Open the input file and import the workload */
    original_trace = new UserSubmitQueue(user_name, input_json);

    if (original_trace->is_empty())
        date_of_next_submission = DATE_NEVER;
    else
        date_of_next_submission
            = lround(original_trace->top()->submission_time);
}

ReplayUser::~ReplayUser()
{
}

void ReplayUser::jobs_to_submit(double date, std::list<shared_ptr<Job>> &jobs,
    std::list<const Profile *> &profiles)
{
    jobs = std::list<shared_ptr<Job>>();
    profiles = std::list<const Profile *>();

    PPK_ASSERT_ERROR(!original_trace->is_empty(),
        "User %s has been called to sumbit but her job queue is empty",
        user_name.c_str());
    PPK_ASSERT_ERROR(
        lround(original_trace->top()->submission_time) == lround(date),
        "First job in user %s's queue has different sumbmission time (%f) than "
        "current date (%f)",
        user_name.c_str(), original_trace->top()->submission_time, date);

    /* Submit all jobs that have same submit time */
    while (!original_trace->is_empty()
        && lround(original_trace->top()->submission_time) == lround(date))
    {
        /* Delete job from the original queue */
        shared_ptr<Job> job = shared_ptr<Job>(new Job(*original_trace->pop()));
        Profile *job_profile = new Profile();

        /* Handle the job according to the user behavior. */
        bool execute_now = handle_job(date, job, job_profile);

        if (execute_now)
        {
            /* Add it to the list of jobs (and profiles, if new) to submit */
            jobs.push_back(job);
            if (sent_profiles.count(job->profile) == 0)
            {
                profiles.push_back(job_profile);
                sent_profiles.insert(job->profile);
            }
        }
    }

    if (original_trace->is_empty())
        date_of_next_submission = DATE_NEVER;
    else
        date_of_next_submission
            = lround(original_trace->top()->submission_time);
}

void ReplayUser::log_behavior(shared_ptr<Job> &job, std::string behavior_name,
    long delay_time, double random_value)
{
    if (logger)
    {
        logger->add_stat(
            job, std::move(behavior_name), delay_time, random_value);
    }
}

/* ReplayUserRigid */
ReplayUserRigid::ReplayUserRigid(
    std::string name, const rapidjson::Value &param, DMWindow *window)
{
    dm_window = window;
    init_ReplayUser(name, param);
}

ReplayUserRigid::~ReplayUserRigid()
{
    delete original_trace;
}

void ReplayUserRigid::jobs_to_submit(double date,
    std::list<shared_ptr<Job>> &jobs, std::list<const Profile *> &profiles)
{
    ReplayUser::jobs_to_submit(date, jobs, profiles);
}

bool ReplayUserRigid::handle_job(
    double date, shared_ptr<Job> job, Profile *profile)
{

    /* If demand response mode: use behavior function handling the log etc. */
    if (dm_window && dm_window->date_in_dm_window(date))
    {
        return rigid_job(job, profile, this, -1);
    }

    /* Otherwise: just generate the corresponding profile by hand */
    Parser::profile_from_duration(profile, job->profile, user_name);
    return true;
}

/******************************************************************************
 *********************** Users "demand response" ******************************
 *****************************************************************************/
DMWindow::DMWindow(int a, int b)
{
    PPK_ASSERT_ERROR(a < b,
        "Invalid demand response window: the lower bound %d should be "
        "lower "
        "than the higher bound %d",
        a, b);
    inf = a;
    sup = b;
}

bool DMWindow::date_in_dm_window(double date)
{
    return date >= inf && date < sup;
}

/* DMUserReconfig */
DMUserReconfig::DMUserReconfig(
    std::string name, const rapidjson::Value &param, DMWindow *window)
{
    dm_window = window;

    /* Fetch the value for alpha if it exists */
    if (param.HasMember("alpha_speedup"))
    {
        PPK_ASSERT_ERROR(param["alpha_speedup"].IsDouble(),
            "Invalid user_description file: 'alpha_speedup' should be a double "
            "for user %s",
            name.c_str());
        alpha = param["alpha_speedup"].GetDouble();
    }

    init_ReplayUser(name, param);
}

DMUserReconfig::~DMUserReconfig()
{
    if (original_trace)
    {
        delete original_trace;
        original_trace = NULL;
    }
}

void DMUserReconfig::jobs_to_submit(double date,
    std::list<shared_ptr<Job>> &jobs, std::list<const Profile *> &profiles)
{
    ReplayUser::jobs_to_submit(date, jobs, profiles);
}

/* DMUserDegradSpace */
DMUserDegradSpace::DMUserDegradSpace(
    std::string name, const rapidjson::Value &param, DMWindow *window)
{
    dm_window = window;
    init_ReplayUser(name, param);
}

DMUserDegradSpace::~DMUserDegradSpace()
{
    if (original_trace)
    {
        delete original_trace;
        original_trace = NULL;
    }
}

void DMUserDegradSpace::jobs_to_submit(double date, std::list<shared_ptr<Job>> &jobs,
    std::list<const Profile *> &profiles)
{
    ReplayUser::jobs_to_submit(date, jobs, profiles);
}

/* DMUserDegradTemp */
DMUserDegradTemp::DMUserDegradTemp(
    std::string name, const rapidjson::Value &param, DMWindow *window)
{
    dm_window = window;
    init_ReplayUser(name, param);
}

DMUserDegradTemp::~DMUserDegradTemp()
{
    if (original_trace)
    {
        delete original_trace;
        original_trace = NULL;
    }
}

void DMUserDegradTemp::jobs_to_submit(double date, std::list<shared_ptr<Job>> &jobs,
    std::list<const Profile *> &profiles)
{
    ReplayUser::jobs_to_submit(date, jobs, profiles);
}

/* DMUserRenounce */
DMUserRenounce::DMUserRenounce(
    std::string name, const rapidjson::Value &param, DMWindow *window)
{
    dm_window = window;
    init_ReplayUser(name, param);
}

DMUserRenounce::~DMUserRenounce()
{
    if (original_trace)
    {
        delete original_trace;
        original_trace = NULL;
    }
}

void DMUserRenounce::jobs_to_submit(double date,
    std::list<shared_ptr<Job>> &jobs, std::list<const Profile *> &profiles)
{
    ReplayUser::jobs_to_submit(date, jobs, profiles);
}

/* DMUserDelay */
DMUserDelay::DMUserDelay(
    std::string name, const rapidjson::Value &param, DMWindow *window)
{
    dm_window = window;
    init_ReplayUser(name, param);
}

DMUserDelay::~DMUserDelay()
{
    if (original_trace)
    {
        delete original_trace;
        original_trace = NULL;
    }
}

void DMUserDelay::jobs_to_submit(double date, std::list<shared_ptr<Job>> &jobs,
    std::list<const Profile *> &profiles)
{
    ReplayUser::jobs_to_submit(date, jobs, profiles);
}

bool DMUserReconfig::handle_job(
    double date, shared_ptr<Job> job, Profile *profile)
{
    if (!dm_window->date_in_dm_window(date))
    {
        Parser::profile_from_duration(profile, job->profile, user_name);
        return true;
    }

    this->dm_stat[2 * CONSIDER_RECONF]++;
    if (job->nb_requested_resources > 1)
        return reconfig_job(job, profile, alpha, this, -1.0);

    return rigid_job(job, profile, this, -1.0);
}

bool DMUserDegradSpace::handle_job(
    double date, shared_ptr<Job> job, Profile *profile)
{
    if (!dm_window->date_in_dm_window(date))
    {
        Parser::profile_from_duration(profile, job->profile, user_name);
        return true;
    }

    this->dm_stat[2 * CONSIDER_DEGRADED]++;
    if (job->nb_requested_resources > 1)
        return degrad_space_job(job, profile, this, -1);

    return rigid_job(job, profile, this, -1);
}

bool DMUserDegradTemp::handle_job(
    double date, shared_ptr<Job> job, Profile *profile)
{
    if (!dm_window->date_in_dm_window(date))
    {
        Parser::profile_from_duration(profile, job->profile, user_name);
        return true;
    }

    return degrad_temp_job(job, profile, this, -1);
}

bool DMUserRenounce::handle_job(
    double date, shared_ptr<Job> job, Profile *profile)
{
    if (dm_window->date_in_dm_window(date))
        return renounce_job(job, this, -1);

    Parser::profile_from_duration(profile, job->profile, user_name);
    return true;
}

bool DMUserDelay::handle_job(double date, shared_ptr<Job> job, Profile *profile)
{
    if (dm_window->date_in_dm_window(date))
        return delay_job(dm_window->sup, job, original_trace, this, -1, false);

    Parser::profile_from_duration(profile, job->profile, user_name);
    return true;
}
