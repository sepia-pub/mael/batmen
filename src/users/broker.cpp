#include "broker.hpp"
#include "json_workload.hpp"
#include "response_behaviors.hpp"
#include "users/user.hpp"

#include <boost/format.hpp>
#include <fstream>

#include "../pempek_assert.hpp"
#include "rapidjson/rapidjson.h"
#include <loguru.hpp>
#include <memory>
using namespace rapidjson;

Broker::Broker(rapidjson::Document *user_description_file)
{
    /* Parse description file and call constructor for each user */
    if (!user_description_file->ObjectEmpty())
    {
        if (user_description_file->HasMember("core_limit_per_user"))
        {
            PPK_ASSERT_ERROR(
                (*user_description_file)["core_limit_per_user"].IsInt(),
                "Invalid user_description file: field "
                "'core_limit_per_user' should be an int.");
            core_limit_per_user
                = (*user_description_file)["core_limit_per_user"].GetInt();
            // otherwise, initialized at std::numeric_limits<int>::max()
        }

        /* demand reponse related windows parsing */
        if (user_description_file->HasMember("dm_window"))
        {
            const Value &dm_param = (*user_description_file)["dm_window"];
            PPK_ASSERT_ERROR(dm_param.IsArray()
                    && dm_param.GetArray().Size() == 2
                    && dm_param.GetArray()[0].IsInt()
                    && dm_param.GetArray()[1].IsInt(),
                "Invalid user_description file: field "
                "'dm_window' should be an array of int of size 2.");
            dm_window = new DMWindow(dm_param.GetArray()[0].GetInt(),
                dm_param.GetArray()[1].GetInt());
        }
        if (user_description_file->HasMember("yellow_windows"))
        {
            yellow_windows_list
                = parse_dm_windows("yellow_windows", user_description_file);
        }
        if (user_description_file->HasMember("red_windows"))
        {
            red_windows_list
                = parse_dm_windows("red_windows", user_description_file);
        }

        if (red_windows_list && yellow_windows_list)
        {
            PPK_ASSERT_ERROR(
                red_windows_list->have_no_common_element(yellow_windows_list),
                "Error red_windows field and yellow_windows field have at "
                "least one common element in their interval");
        }
        state_automata = new DMWindowAutomata(
            dm_window, red_windows_list, yellow_windows_list);

        // seed  parameter parsing
        bool seed_defined = false;
        if (user_description_file->HasMember("seed"))
        {
            const Value &seed_param = (*user_description_file)["seed"];
            PPK_ASSERT_ERROR(seed_param.IsInt(),
                "Invalid user_description file : field "
                "'seed' should be a 32-bit integer");
            int seed = seed_param.GetInt();
            seed_generator = mt19937(seed);
            seed_defined = true;
        }

        // logging info parsing
        if (user_description_file->HasMember("log_user_stats"))
        {
            PPK_ASSERT_ERROR(
                (*user_description_file)["log_user_stats"].IsBool(),
                "Invalid user_description file : field"
                "'log_user_stats' should be a boolean");
            bool log_user_stats
                = (*user_description_file)["log_user_stats"].GetBool();
            if (log_user_stats)
            {
                PPK_ASSERT_ERROR(user_description_file->HasMember("log_folder"),
                    "Invalid user_description file : field"
                    "'log_folder' should be defined with 'log_user_stats' ");
                const Value &log_folder_param
                    = (*user_description_file)["log_folder"];
                PPK_ASSERT_ERROR(log_folder_param.IsString(),
                    "Invalid user_description file : field"
                    " 'log_folder' should be a  string ");
                std::string log_folder = log_folder_param.GetString();
                logger_user_stat = new LoggerUserStat(log_folder);
            }
        }
        PPK_ASSERT_ERROR(user_description_file->HasMember("users"),
            "Invalid user_description file: should have field 'users'.");
        const Value &users_json = (*user_description_file)["users"];
        PPK_ASSERT_ERROR(users_json.IsArray(),
            "Invalid user_description file: field 'users' should be an array.");

        for (SizeType i = 0; i < users_json.Size(); i++)
        {
            PPK_ASSERT_ERROR(users_json[i].HasMember("name")
                    && users_json[i]["name"].IsString(),
                "Invalid user_description file: user should have string field "
                "'name'.");
            string name = users_json[i]["name"].GetString();

            PPK_ASSERT_ERROR(users_json[i].HasMember("category")
                    && users_json[i]["category"].IsString(),
                "Invalid user_description file: user should have string field "
                "'category'.");
            string category = users_json[i]["category"].GetString();

            PPK_ASSERT_ERROR(users_json[i].HasMember("param")
                    && users_json[i]["param"].IsObject(),
                "Invalid user_description file: user should have json field "
                "'param'.");
            const Value &param = users_json[i]["param"];

            User *user;
            if (category == "routine_greedy")
                user = new RoutineGreedyUser(name, param);

            else if (category == "dicho_intersubmission_time")
                user = new DichoIntersubmitTimeUser(name, param);

            else if (category == "think_time_user")
                user = new ThinkTimeUser(name, param);

            else if (category == "replay_user_rigid")
                user = new ReplayUserRigid(name, param, dm_window);

            /* DM user */
            else if (category == "dm_user_reconfig"
                || category == "dm_user_degrad_temp"
                || category == "dm_user_degrad_space"
                || category == "dm_user_renounce"
                || category == "dm_user_delay")
            {
                PPK_ASSERT_ERROR(dm_window != nullptr,
                    "User %s is a demand response user. The field 'dm_window' "
                    "should be defined in the user_description_file.",
                    name.c_str());

                if (category == "dm_user_reconfig")
                    user = new DMUserReconfig(name, param, dm_window);

                else if (category == "dm_user_degrad_space")
                    user = new DMUserDegradSpace(name, param, dm_window);

                else if (category == "dm_user_degrad_temp")
                    user = new DMUserDegradTemp(name, param, dm_window);

                else if (category == "dm_user_renounce")
                    user = new DMUserRenounce(name, param, dm_window);

                else // if (category == "dm_user_delay")
                    user = new DMUserDelay(name, param, dm_window);
            }

            else if (category == "dm_user_multi_behavior")
            {
                PPK_ASSERT_ERROR(
                    dm_window || red_windows_list || yellow_windows_list,
                    "One of these 3 fields should be defined to use "
                    "dm_user_multi_behavior"
                    " 'dm_windows' 'red_windows', 'yellow_windows' ");
                PPK_ASSERT_ERROR(seed_defined,
                    "No field 'seed' defined although dm_user_multi_behavior "
                    "needs it");
                user = new DMUserMultiBehavior(name, param, seed_generator(),
                    state_automata, logger_user_stat);
            }
            else if (category == "dm_user_degrad")
            {
                PPK_ASSERT_ERROR(false,
                    "Invalid user_description file: dm_user_degrad has been "
                    "removed since October 2023. Consider using "
                    "dm_user_degrad_time or dm_user_degrad_space.");
                user = nullptr;
            }
            else if (category == "dm_user_renonce")
            {
                PPK_ASSERT_ERROR(false,
                    "Invalid user_description file: 'dm_user_renonce' has been "
                    "ranamed 'dm_user_renounce' since October 2023.");
                user = nullptr;
            }

            /* Feedback user */
            else if (category == "fb_user_think_time_only")
                user = new FBUserThinkTimeOnly(name, param);

            else
            {
                PPK_ASSERT_ERROR(false,
                    "Invalid user_description file: unknown user category.");
                user = nullptr;
            }

            users.emplace(user->user_name, user);
        }
    }

    /* Create users by hand */
    else
    {
        users.emplace(
            "geo", new RoutineGreedyUser("geo", 0.0, 10000.0, 1000.0, 2));

        // alice and bob
        users.emplace("alice",
            new DichoIntersubmitTimeUser("alice", 0.0, 10000.0, 1000.0));
        users.emplace(
            "bob", new DichoIntersubmitTimeUser("bob", 500.0, 10000.0, 1000.0));
    }

    // int nb_users = 2;
    // for (int i=0; i<nb_users; i++)
    // {
    //     string user_name = "user" + to_string(i)
    //     users.push_back(new DichoIntersubmitTimeUser(user_name, ));

    // }

    for (auto it : users)
    {
        user_queue.push_back(it.second);
    }
    user_queue.sort(CompareUsers());
}

Broker::~Broker()
{
    for (auto it : users)
    {
        delete it.second;
    }

    dynamic_jobs.clear();
    users_to_wake.clear();
}

long Broker::next_submission(double date) const
{
    return user_queue.front()->next_submission();
}

void Broker::jobs_to_submit(
    double date, list<Job> &jobs, list<const Profile *> &profiles)
{
    jobs = list<Job>();
    profiles = list<const Profile *>();

    User *user = user_queue.front();
    long planned_date_submission = user->next_submission();
    PPK_ASSERT_WARNING(date - planned_date_submission < 1.0,
        "The user asked to be called at date=%lu which is more than 1sec "
        "earlier than now",
        planned_date_submission);

    /* There might be more than one user wanting to submit at this date */
    while (planned_date_submission == user->next_submission())
    {
        user_queue.pop_front();
        unsigned int nb_core_requested = 0;
        list<shared_ptr<Job>> user_jobs;
        list<const Profile *> user_profiles;
        user->jobs_to_submit(date, user_jobs, user_profiles);

        for (shared_ptr<Job> job : user_jobs)
        {
            nb_core_requested = nb_core_requested + job->nb_requested_resources;

            if (nb_core_requested > core_limit_per_user)
            {
                job->status = KILLED;
            }
            else
            {
                jobs.push_back(*job);
                dynamic_jobs[job->id] = job;
                job->status = WAITING;
            }
        }
        profiles.splice(profiles.end(), user_profiles);

        /* user->next_submission has been updated by side effeect */
        user_queue.push_back(user);
        user = user_queue.front();
    }

    /* Reorder the user queue */
    user_queue.sort(CompareUsers());

    /* Sort the jobs before sending them to submission, for determinism */
    jobs.sort(JobComparator());
}

void Broker::feedback_job_status(double date, vector<string> &jobs_ended,
    vector<string> &jobs_killed, vector<string> &jobs_released)
{
    /* Jobs ended recently */
    for (const string &job_id : jobs_ended)
    {
        update_status_if_dyn_job(job_id, FINISHED);
    }

    /* Jobs killed recently */
    for (const string &job_id : jobs_killed)
    {
        update_status_if_dyn_job(job_id, KILLED);
    }

    /* Wake up affected users to give them feedback */
    for (auto userJobs : users_to_wake)
    {
        userJobs.first->wake_on_feedback(date, userJobs.second);
    }
    users_to_wake.clear();

    /* Dates of next submission may have changed in response to feedback: sort
     * the queue */
    user_queue.sort(CompareUsers());
}

DMWindow_list *Broker::parse_dm_windows(const std::string &attr_name,
    const rapidjson::Document *user_description_file)
{
    const Value &windows_param = (*user_description_file)[attr_name.c_str()];
    std::string error_message = "Invalid user_description file: field ";
    error_message += attr_name;
    error_message += " should be a non-empty array";
    PPK_ASSERT_ERROR(
        windows_param.IsArray() && windows_param.GetArray().Size() != 0, "%s",
        error_message.c_str());
    error_message = "Invalid user_description file: the field ";
    error_message += attr_name;
    error_message += " should be an array of intervals (pairs of integers).";
    std::string error_message_sorted_disjoint
        = "Invalid user_description file : the field ";
    error_message_sorted_disjoint += attr_name;
    error_message_sorted_disjoint
        += " should be sorted and contains disjoint interval";
    IntervalSet windows_set;
    int last_bound = -1;
    for (SizeType i = 0; i < windows_param.GetArray().Size(); i++)
    {
        const Value &window
            = (*user_description_file)[attr_name.c_str()].GetArray()[i];
        PPK_ASSERT_ERROR(window.IsArray() && window.GetArray().Size() == 2
                && window.GetArray()[0].IsInt() && window.GetArray()[1].IsInt(),
            "%s", error_message.c_str());
        int begin_window = window.GetArray()[0].GetInt();
        int end_window = window.GetArray()[1].GetInt();
        PPK_ASSERT_ERROR(begin_window > last_bound, "%s",
            error_message_sorted_disjoint.c_str());
        last_bound = end_window;
        IntervalSet to_add
            = IntervalSet::ClosedInterval(begin_window, end_window);
        windows_set += to_add;
    }
    return new DMWindow_list(windows_set);
}
void Broker::update_status_if_dyn_job(const string &job_id, JobStatus status)
{
    auto it = dynamic_jobs.find(job_id);
    if (it != dynamic_jobs.end())
    {
        string user_name = job_id.substr(0, job_id.find('!'));
        User *user = users[user_name];
        shared_ptr<Job> current_job = it->second;

        switch (status)
        {
        case WAITING:
            PPK_ASSERT_ERROR(current_job->status == WAITING);
            break;
        case RUNNING:
            PPK_ASSERT_ERROR(current_job->status == WAITING);
            current_job->status = RUNNING;
            LOG_F(2, "Updating status of job %s: RUNNING", it->first.c_str());
            break;
        case FINISHED:
            PPK_ASSERT_ERROR(current_job->status == RUNNING);
            current_job->status = FINISHED;
            LOG_F(2, "Updating status of job %s: FINISHED", it->first.c_str());

            /* Add potentially interested user to the map of users to wake up */
            if (users_to_wake.find(user) == users_to_wake.end())
                users_to_wake.emplace(user, list<shared_ptr<Job>>());

            /* ..and keep track of its recently ended jobs */
            users_to_wake[user].push_back(current_job);

            dynamic_jobs.erase(it); // job ended, free memory
            break;
        case KILLED:
            PPK_ASSERT_ERROR(current_job->status == WAITING
                || current_job->status == RUNNING);
            current_job->status = KILLED;
            LOG_F(2, "Updating status of job %s: KILLED", it->first.c_str());

            /* Add potentially interested user to the map of users to wake up */
            if (users_to_wake.find(user) == users_to_wake.end())
                users_to_wake.emplace(user, list<shared_ptr<Job>>());

            /* ..and keep track of its recently ended jobs */
            users_to_wake[user].push_back(current_job);

            dynamic_jobs.erase(it); // job ended, free memory
            break;
        }
    }
}

void Broker::log_user_stats(string log_folder)
{
    static long stat[14] = { 0 };
    for (auto it : users)
    {
        User *user = it.second;
        for (int i = 0; i < 14; i++)
            stat[i] += user->dm_stat[i];
    }
    if (logger_user_stat)
    {
        logger_user_stat->log_stat();
    }
    /* Write in file */
    ofstream file(log_folder + "/user_stats.csv");

    file << ",nb_jobs,core_seconds\n";
    file << boost::format("rigid,%1%,%2%\n") % stat[2 * RIGID]
            % stat[2 * RIGID + 1];
    file << boost::format("renounced,%1%,%2%\n") % stat[2 * RENOUNCED]
            % stat[2 * RENOUNCED + 1];
    file << boost::format("delayed,%1%,%2%\n") % stat[2 * DELAYED]
            % stat[2 * DELAYED + 1];
    file << boost::format("degraded,%1%,%2%\n") % stat[2 * DEGRADED]
            % stat[2 * DEGRADED + 1];
    file << boost::format("reconf,%1%,%2%\n") % stat[2 * RECONF]
            % stat[2 * RECONF + 1];
    file << boost::format("consider degraded,%1%,%2%\n")
            % stat[2 * CONSIDER_DEGRADED] % stat[2 * CONSIDER_DEGRADED + 1];
    file << boost::format("consider reconfig,%1%,%2%\n")
            % stat[2 * CONSIDER_RECONF] % stat[2 * CONSIDER_RECONF + 1];

    file.close();
}