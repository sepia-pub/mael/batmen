/**
 * Users simulated by replaying an input trace.
 */
#pragma once
#include "json_workload.hpp"
#include "queue.hpp"
#include "users/log_user_stat.hpp"
#include "users/user.hpp"
#include <random>
/**
 * The demand response window during which the users are enclined to reduce
 * their consumption. (semi-closed interval [inf, sup[)
 */
struct DMWindow
{
    int inf;
    int sup;
    DMWindow(int a, int b);
    bool date_in_dm_window(double date);
};

/**
 * @brief Virtual class for users using a trace as input for submission.
 */
class ReplayUser : public User
{
public:
    virtual ~ReplayUser();
    long next_submission();
    virtual void jobs_to_submit(double date, std::list<shared_ptr<Job>> &jobs,
        std::list<const Profile *> &profiles);
    void log_behavior(shared_ptr<Job> &job, std::string behavior_name,
        long delay_time, double random_value);

protected:
    /**
     * To be called by the constructor of each ReplayUser children.
     * Initialize user name and variables associated with handling the
     * original trace.
     */
    void init_ReplayUser(std::string name, const rapidjson::Value &param);

    /**
     * A job is read in the original trace. Handle it depending on the user
     * behavior and returns if it should still be executed now.
     */
    virtual bool handle_job(double date, shared_ptr<Job> job, Profile *profile)
        = 0;

public:
    long dm_stat[14] = { 0 };

protected:
    std::string input_json;
    UserSubmitQueue *original_trace;
    DMWindow *dm_window = nullptr;

    LoggerUserStat *logger = nullptr;
    /* Keep track of profiles to not send them twice to Batsim */
    std::set<std::string> sent_profiles;
};

/**
 * @brief The user follows the input trace in a rigid fashion: she submits tasks
 * of same size and at the same submission time as in the original trace.
 */
class ReplayUserRigid : public ReplayUser
{
public:
    ReplayUserRigid(
        std::string name, const rapidjson::Value &param, DMWindow *window);
    ~ReplayUserRigid();
    long next_submission();
    void jobs_to_submit(double date, std::list<shared_ptr<Job>> &jobs,
        std::list<const Profile *> &profiles);

protected:
    bool handle_job(double date, shared_ptr<Job> job, Profile *profile);
};

/******************************************************************************
 *********************** Users "demand response" ******************************
 *****************************************************************************/
/**
 * @brief The user follows the input trace in a rigid fashion, except in the
 * demand response window where she reconfigures the submitted jobs.
 * @details The reconfiguration consists in dividing the number of cores
 * requested by two (rounded up) and scaling the execution time accordingly,
 * following the speedup model. Note: if the original number of cores requested
 * was 1, reconfig is equivalent to rigid, and will appear as such in the logs.
 */
class DMUserReconfig : virtual public ReplayUser
{
public:
    DMUserReconfig(
        std::string name, const rapidjson::Value &param, DMWindow *dm_window);
    ~DMUserReconfig();
    long next_submission();
    void jobs_to_submit(double date, std::list<shared_ptr<Job>> &jobs,
        std::list<const Profile *> &profiles);

protected:
    virtual bool handle_job(double date, shared_ptr<Job> job, Profile *profile);
    double alpha = 1.0; // for the speedup model
};

/**
 * @brief The user follows the input trace in a rigid fashion, except in the
 * demand response window where she degrades spatially the submitted jobs.
 * @details The spatial degradation consists in dividing the number of cores requested
 * by two (rounded up), keeping the original submission time. Note: if the
 * original number of cores requested was 1, degrad is equivalent to rigid, and
 * will appear as such in the logs.
 */
class DMUserDegradSpace : virtual public ReplayUser
{
public:
    DMUserDegradSpace(
        std::string name, const rapidjson::Value &param, DMWindow *dm_window);
    ~DMUserDegradSpace();
    long next_submission();
    void jobs_to_submit(double date, std::list<shared_ptr<Job>> &jobs,
        std::list<const Profile *> &profiles);

protected:
    bool handle_job(double date, shared_ptr<Job> job, Profile *profile);
};

/**
 * @brief The user follows the input trace in a rigid fashion, except in the
 * demand response window where she degrades temporally the submitted jobs.
 * @details The temporal degradation consists in dividing the execution time
 * by two.
 */
class DMUserDegradTemp : virtual public ReplayUser
{
public:
    DMUserDegradTemp(
        std::string name, const rapidjson::Value &param, DMWindow *dm_window);
    ~DMUserDegradTemp();
    long next_submission();
    void jobs_to_submit(double date, std::list<shared_ptr<Job>> &jobs,
        std::list<const Profile *> &profiles);

protected:
    bool handle_job(double date, shared_ptr<Job> job, Profile *profile);
};

/**
 * @brief The user follows the input trace in a rigid fashion, except in the
 * demand response window where she renounces the jobs.
 * @details Renouncing consists in simply not submitting the job (and never
 * submit it in the future).
 */
class DMUserRenounce : virtual public ReplayUser
{
public:
    DMUserRenounce(
        std::string name, const rapidjson::Value &param, DMWindow *dm_window);
    ~DMUserRenounce();
    long next_submission();
    void jobs_to_submit(double date, std::list<shared_ptr<Job>> &jobs,
        std::list<const Profile *> &profiles);

protected:
    bool handle_job(double date, shared_ptr<Job> job, Profile *profile);
};

/**
 * @brief The user follows the input trace in a rigid fashion, except in the
 * demand response window where she delays the submitted jobs.
 * @details Delaying consists in not submitting the job straight away but at the
 * end of the demand response window.
 */
class DMUserDelay : virtual public ReplayUser
{
public:
    DMUserDelay(
        std::string name, const rapidjson::Value &param, DMWindow *dm_window);
    ~DMUserDelay();
    long next_submission();
    void jobs_to_submit(double date, std::list<shared_ptr<Job>> &jobs,
        std::list<const Profile *> &profiles);

protected:
    bool handle_job(double date, shared_ptr<Job> job, Profile *profile);
};
