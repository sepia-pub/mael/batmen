#include "dynscheduler.hpp"
#include "isalgorithm.hpp"

#include "../pempek_assert.hpp"
#include "json_workload.hpp"
#include "loguru.hpp"
#include <cmath>
#include <limits>
#include <map>
#include <math.h>
#include <string>

DynScheduler::DynScheduler(Workload *workload, SchedulingDecision *decision,
    Queue *queue, ResourceSelector *selector, double rjms_delay,
    rapidjson::Document *variant_options)
    : ISchedulingAlgorithm(
        workload, decision, queue, selector, rjms_delay, variant_options)
{
}

DynScheduler::~DynScheduler()
{
    delete broker;
}

void DynScheduler::on_simulation_start(
    double date, const rapidjson::Value &batsim_config)
{
    /* Enable broker mode if dynamic jobs are enabled */
    // TODO: give a better name to batsim CLI like "--broker-mode" */
    broker_enabled = batsim_config["dynamic-jobs-enabled"].GetBool();
    if (broker_enabled)
    {
        PPK_ASSERT_ERROR(batsim_config["dynamic-jobs-acknowledged"].GetBool(),
            "Dynamic jobs are enabled, please also add the option "
            "--acknowledge-dynamic-jobs.");
        PPK_ASSERT_ERROR(batsim_config["profile-reuse-enabled"].GetBool(),
            "Dynamic jobs are enabled, please also add the option "
            "--enable-profile-reuse.");

        broker = new Broker(_variant_options);

        /* Check for optional parameters */
        if (!_variant_options->ObjectEmpty())
        {
            if (_variant_options->HasMember("log_user_stats")
                && (*_variant_options)["log_user_stats"].GetBool())
            {
                PPK_ASSERT_ERROR(_variant_options->HasMember("log_folder")
                        && (*_variant_options)["log_folder"].IsString(),
                    "Invalid user_description file: you should provide a path "
                    "for the user_stat file.");
                log_folder = (*_variant_options)["log_folder"].GetString();
            }
        }

        date_of_next_submission = broker->next_submission(date);

        /* Case jobs to submit at date=0: handle them */
        if (date_of_next_submission == 0.0)
        {
            submit_broker_jobs(date);

            /* Update date of next submission */
            date_of_next_submission = broker->next_submission(date);
        }

        /* No (more) jobs to submit.. */
        if (date_of_next_submission == DATE_NEVER)
            end_broker(date);

        /* Otherwise call the broker later */
        else if (date_of_next_submission != DATE_UNKNOWN)
            _decision->add_call_me_later(date_of_next_submission, date);
    }
}

void DynScheduler::make_decisions(double date,
    SortableJobOrder::UpdateInformation *update_info,
    SortableJobOrder::CompareInformation *compare_info)
{
    if (broker_enabled)
    {
        /* Send feedback to interested users */
        broker->feedback_job_status(date, _jobs_ended_recently,
            _jobs_killed_recently, _jobs_released_recently);

        /* Time of next_submission might have changed in response to feedback */
        double new_date_of_next_submission = broker->next_submission(date);

        if (new_date_of_next_submission == lround(date))
        {
            submit_broker_jobs(date);
            new_date_of_next_submission = broker->next_submission(date);
        }

        if (new_date_of_next_submission != date_of_next_submission)
        {
            date_of_next_submission = new_date_of_next_submission;
            next_submission_has_changed = true;
        }

        /* Call back the broker at the next submission */
        if (next_submission_has_changed)
        {
            if (date_of_next_submission == DATE_NEVER)
                end_broker(date);
            else if (date_of_next_submission != DATE_UNKNOWN)
                _decision->add_call_me_later(date_of_next_submission, date);
            next_submission_has_changed = false;
        }
    }
}

void DynScheduler::on_requested_call(double date)
{
    ISchedulingAlgorithm::on_requested_call(date);

    /* Check if this call concerns the broker */
    if (broker_enabled && broker->next_submission(date) == lround(date))
    {
        /* Handle the jobs to submit */
        submit_broker_jobs(date);

        /* Ask for the next requested call */
        date_of_next_submission = broker->next_submission(date);
        next_submission_has_changed = true;
    }
}

void DynScheduler::end_broker(double date)
{
    _decision->add_scheduler_finished_submitting_jobs(date);

    if (log_folder != "")
    {
        broker->log_user_stats(log_folder);
    }
}

void DynScheduler::submit_broker_jobs(double date)
{
    std::list<Job> jobs;
    std::list<const Profile *> profiles;
    broker->jobs_to_submit(date, jobs, profiles);

    if (!profiles.empty()) {
        dyn_register_profiles(profiles, date);

        /* Profiles won't be used anymore, free memory */
        for (const Profile *profile : profiles){
            delete profile;
        }
    }
    dyn_submit(jobs, date);
}

void DynScheduler::dyn_submit(std::list<Job> jobs, double date)
{
    for (Job current_job : jobs)
    {
        std::string fullID = current_job.id;
        PPK_ASSERT_ERROR(fullID.find('!') != std::string::npos,
            "Dynamically submitted jobs should have the format "
            "'user_name!jobID'.");
        std::string user_name = fullID.substr(0, fullID.find('!'));
        fullID.erase(0, fullID.find('!') + 1);
        std::string jobID = fullID;
        std::string job_description
            = Parser::json_description_string_from_job(&current_job);
        std::string empty_profile_description
            = std::string(); // has already been sent

        /* Register the job to batsim */
        _decision->add_submit_job(user_name, jobID, current_job.profile,
            job_description, empty_profile_description, date, true);

    }
}

void DynScheduler::dyn_register_profiles(
    std::list<const Profile *> profiles, double date)
{
    for (auto profile : profiles)
    {
        _decision->add_submit_profile(
            profile->workload_name, profile->name, profile->description, date);
    }
}