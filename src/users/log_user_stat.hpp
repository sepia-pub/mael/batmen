#pragma once

#include "json_workload.hpp"
#include <boost/format.hpp>
#include <fstream>
/**
 * library dedicated to logging user_behavior
 */
using namespace std;

struct BehaviorStat {
    shared_ptr <Job> job;
    std::string behavior_name;
    long time_delayed;
    double random_value;
    BehaviorStat(shared_ptr <Job> job,
                 std::string behavior_name, long time_delayed,double random_value);
    std::vector <std::string> split_id();
    boost::basic_format<char> format();
};

class LoggerUserStat {
public:
    LoggerUserStat(std::string log_folder);
    void add_stat(const shared_ptr <Job> & job,
        std::string behavior_name, long time_delayed, double random_value);
    void log_stat();

protected:
    bool put_header;
    std::vector<BehaviorStat>::size_type write_threshold;
    std::string log_folder;
    std::vector <BehaviorStat> behaviors;
};