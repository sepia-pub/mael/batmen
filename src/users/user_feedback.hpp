/**
 * Users simulated by replaying with feedback an input trace.
 */
#include "users/user.hpp"
#include "json_workload.hpp"
#include <cstddef>
#include <queue>
#include <vector>

/**
 * @brief A user that replays an input trace, adapting the submission times to
 * the status of previously submitted jobs.
 */
class FeedbackUser : public User
{
public:
    virtual ~FeedbackUser();
    long next_submission();
    virtual void jobs_to_submit(
        double date, std::list<shared_ptr<Job>> &jobs, std::list<const Profile *> &profiles);
    virtual void wake_on_feedback(double date, std::list<shared_ptr<Job>> &ended_jobs);

protected:
    /**
     * To be called by the constructor of each FeedbackUser children.
     * Initialize user name and variables associated with handling the original
     * trace.
     */
    void init_FeedbackUser(std::string name, const rapidjson::Value &param);

    /**
     * Method to update the date of the next submission of the user
     */
    void update_date_next_sub();

    /**
     * A session has finished (all its jobs finished). Update its following
     * sessions and the free sessions depending on the replay model.
     */
    virtual void close_session(double date, Session *finished_session) = 0;

    /**
     * A job has to be submitted according to the replay model. Handle it
     * depending on the user behavior and returns if it should still be executed
     * now.
     */
    virtual bool handle_job(double date, shared_ptr<Job>job, Profile *profile) = 0;

protected:
    std::string input_json;
    /* Keep track of profiles to not send them twice to Batsim */
    std::set<std::string> sent_profiles;
    unsigned int ongoing_job_counter = 0;

    /* A session is "active" if the user is currently submitting from it */
    std::list<Session *> active_sessions;

    /* A session is "free" if its dependancy list is empty and it hasn't started
     * yet */
    std::priority_queue<Session *, std::vector<Session *>, SessionComparator>
        free_sessions;
};

/**
 * @brief A user that replays an input trace, preserving the think time between
 * sessions.
 * @details The input trace is in Session Annotated Batsim JSON (SABjson)
 * format, containing the original think times between sessions. The user starts
 * to submit the jobs of the next session as soon as the last think time after
 * all preceding sessions have elapsed.
 */
class FBUserThinkTimeOnly : public FeedbackUser
{
public:
    FBUserThinkTimeOnly(std::string name, const rapidjson::Value &param);
    ~FBUserThinkTimeOnly();
    // long next_submission();
    // virtual void jobs_to_submit(
    //     double date, std::list<Job *> &jobs, std::list<Profile *> &profiles);

protected:
    void close_session(double date, Session *finished_session);
    bool handle_job(double date, shared_ptr<Job> job, Profile *profile);
};