#include "user.hpp"

#include "../pempek_assert.hpp"
#include <algorithm>
#include <cmath>

#include <loguru.hpp>

User::~User()
{
}

long User::next_submission() const
{
    return date_of_next_submission;
}

void User::wake_on_feedback(double date, std::list<shared_ptr<Job>> &ended_jobs)
{
    return;
}


