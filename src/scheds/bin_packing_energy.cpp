#include "bin_packing_energy.hpp"

#include <iostream>

#include <loguru.hpp>

#include "../pempek_assert.hpp"
#include "scheds/HARD_DEFINED_VAR.hpp"
#include "users/dynscheduler.hpp"
#include "intervalset.hpp"
#include "json_workload.hpp"

BinPackingEnergy::SortableHost::SortableHost(
    int id_host, int cores, MachineState state, int awake_pst, int asleep_pst)
    : id(id_host)
    , nb_cores(cores)
    , machine_state(state)
    , pstate_awake(awake_pst)
    , pstate_asleep(asleep_pst)
{
    nb_available_cores = nb_cores;
    this->booked_jobs_id = new std::list<std::string>();
}

void BinPackingEnergy::SortableHost::update_available_cores(
    int nb_cores_available)
{
    nb_available_cores = nb_cores_available;
}

void BinPackingEnergy::SortableHost::book(std::string job_id)
{
    this->booked_jobs_id->push_back(job_id);
    PPK_ASSERT_ERROR(!this->booked_jobs_id->empty(),
        "Lists of booked jobs is empty after adding job %s", job_id.c_str());
}

bool BinPackingEnergy::SortableHost::is_idle()
{
    return (machine_state == AWAKE) && (nb_available_cores == nb_cores);
}

bool BinPackingEnergy::HostComparator::operator()(
    SortableHost *host1, SortableHost *host2)
/* In the queue of hosts we have first the awake hosts in ascending order of
 * available cores, then the the hosts switching on, asleep and switching off */
{
    if (host1->machine_state == host2->machine_state)
    {
        if (host1->nb_available_cores == host2->nb_available_cores)
            return host1->id < host2->id;
        else
            return host1->nb_available_cores < host2->nb_available_cores;
    }
    return host1->machine_state < host2->machine_state;
}

BinPackingEnergy::BinPackingEnergy(Workload *workload,
    SchedulingDecision *decision, Queue *queue, ResourceSelector *selector,
    double rjms_delay, rapidjson::Document *variant_options)
    : DynScheduler(
        workload, decision, queue, selector, rjms_delay, variant_options)
{
}

BinPackingEnergy::~BinPackingEnergy()
{
    for (auto host : listofHosts)
        delete host;
    listofHosts.clear();
}

std::string BinPackingEnergy::machine_state_to_string(
    BinPackingEnergy::MachineState state)
{
    switch (state)
    {
    case AWAKE:
        return "awake";
    case ASLEEP:
        return "asleep";
    case SWITCHING_OFF:
        return "switching_off";
    case SWITCHING_ON:
        return "switching_on";
    }

    PPK_ASSERT_ERROR(false, "Unhandled case");
    return "unhandled_case";
}

std::string BinPackingEnergy::hosts_to_string(
    std::list<SortableHost *> listofHosts)
{
    std::string s = "{";
    for (auto host : listofHosts)
    {
        s += "(id=" + std::to_string(host->id)
            + ", a_core=" + std::to_string(host->nb_available_cores)
            + ", state=" + machine_state_to_string(host->machine_state) + "),";
    }
    s += "}";
    return s;
}

void BinPackingEnergy::on_simulation_start(
    double date, const rapidjson::Value &batsim_config)
{
    /* Call superclass. If broker enabled, submit jobs date=0. */
    DynScheduler::on_simulation_start(date, batsim_config);

    // TODO: send also an error if --enable-compute-sharing option is not given
    // PPK_ASSERT_ERROR(
    //     batsim_config["dynamic-jobs-enabled"].GetBool(), "This algorithm only
    //     works if dynamic job are enabled!");

    /* TODO (wait for batsim5.0):
     *   - get the initial machine state (AWAKE, ASLEEP, ...) of each machine
     *     from the platform file
     *   - get the pstates from the variant_option file
     */
    MachineState initial_machine_state = INITIAL_MACHINE_STATE;
    int pstate_awake = PSTATE_AWAKE;
    int pstate_asleep = PSTATE_ASLEEP;

    /* Initialize hosts */
    vectorofHosts = std::vector<SortableHost *>(_nb_machines);
    for (int i = 0; i < _nb_machines; i++)
    {
        auto host = new SortableHost(
            i, _nb_cores_per_machine, initial_machine_state, pstate_awake, pstate_asleep);
        listofHosts.push_back(host);
        vectorofHosts[i] = host;
    }
    // debug:
    LOG_F(1, "BinPackingEnergy, list of hosts=%s",
        hosts_to_string(listofHosts).c_str());

    on_simulation_begin = true;
}

void BinPackingEnergy::on_simulation_end(double date)
{
    (void)date;
}

void BinPackingEnergy::make_decisions(double date,
    SortableJobOrder::UpdateInformation *update_info,
    SortableJobOrder::CompareInformation *compare_info)
{
    /* SIMULATION_BEGIN is sent in a dedicated message buffer at t=0 even
    if other messages are also sent at t=0. We wait for these potential other
    messages to be read by calling back the scheduler at time t+epsilon. */
    if (on_simulation_begin)
    {
        _decision->add_call_me_later(date + .0001, date);
        on_simulation_begin = false;
    }

    else
    {
        /**********************************************************************
         * Handle recently ended jobs and update the available machines
         **********************************************************************/
        for (const std::string &ended_job_id : _jobs_ended_recently)
        {
            SortableHost *host = current_allocations[ended_job_id];

            const Job *ended_job = (*_workload)[ended_job_id];
            host->nb_available_cores += ended_job->nb_requested_resources;
            PPK_ASSERT_ERROR(host->nb_available_cores <= host->nb_cores);

            current_allocations.erase(ended_job_id);
        }

        /**********************************************************************
         * Handle the machines that changed state recently
         **********************************************************************/
        for (const auto &iter : _machines_whose_pstate_changed_recently)
        {
            int new_state = iter.first;
            IntervalSet machines = iter.second;

            for (auto it = machines.elements_begin();
                 it != machines.elements_end(); ++it)
            {

                SortableHost *current_host = vectorofHosts.at(*it);

                /* Machine newly awaken: execute the booked jobs */
                if (new_state == current_host->pstate_awake)
                {
                    current_host->machine_state = AWAKE;

                    PPK_ASSERT_ERROR(!current_host->booked_jobs_id->empty(),
                        "Machine %d just became awake but has no job booked.",
                        current_host->id);
                    int check_cores = 0;
                    for (auto it = current_host->booked_jobs_id->begin();
                         it != current_host->booked_jobs_id->end();
                         it = current_host->booked_jobs_id->erase(it))
                    {
                        const Job *booked_job = (*_workload)[*it];
                        check_cores += booked_job->nb_requested_resources;
                        execute_or_book(booked_job, current_host, date);
                    }
                    PPK_ASSERT_ERROR(check_cores
                            == current_host->nb_cores
                                - current_host->nb_available_cores,
                        "Machine %d's number of available cores doesn't match "
                        "the jobs running on it.",
                        current_host->id);
                }

                /* Machine newly asleep: nothing to do */
                else if (new_state == current_host->pstate_asleep)
                {
                    current_host->machine_state = ASLEEP;
                    PPK_ASSERT_ERROR(current_host->nb_available_cores
                            == current_host->nb_cores,
                        "Machine %d is asleep, it should have no core used.",
                        current_host->id);
                }

                else
                    PPK_ASSERT_ERROR(false,
                        "Machine %d just changed its pstate to %d, but this "
                        "pstate is unknown.",
                        current_host->id, new_state);
            }
        }

        /**********************************************************************
         * Handle recently released jobs from worklaod: put them in the
         * queue
         **********************************************************************/
        bool sort_queue = _jobs_released_recently.size() >=10;
        for (const std::string &new_job_id : _jobs_released_recently)
        {
            const Job *new_job = (*_workload)[new_job_id];

            if (new_job->nb_requested_resources > _nb_cores_per_machine)
            {
                _decision->add_reject_job(new_job_id, date);
                if (broker_enabled)
                    broker->update_status_if_dyn_job(new_job_id, KILLED);
            }

            else
            {
                if(sort_queue)
                {
                    _queue->append_job(new_job, update_info);
                }
                else
                {
                    _queue->insert_sorted_job(new_job,update_info,nullptr);
                }
            }
        }
        LOG_F(1, "makedecisions, queue of job has size %d", _queue->nb_jobs());
        if (sort_queue)
        {
            _queue->sort_queue(update_info, compare_info);
        }

        /**********************************************************************
         * Binpacking scheduling decision:
         * Go through the job queue and try to schedule the jobs
         **********************************************************************/
        if (_queue->nb_jobs()>0)
        {
            bin_packing_algo(date);
        }


        /* Check end of simulation */
        if (_no_more_static_job_to_submit_received
            && (!broker_enabled
                || (broker_enabled && broker->next_submission(date) == DATE_NEVER)))
        {
            // Check if all hosts are either idle or asleep
            reached_end_of_simulation = true;
            for (auto host : vectorofHosts)
            {
                if (!(host->is_idle() || host->machine_state == ASLEEP))
                {
                    reached_end_of_simulation = false;
                    break;
                }
            }
        }

        /***********************************************************************
         * Switch off the idle machines
         **********************************************************************/
        if (!reached_end_of_simulation)
            for (auto host : vectorofHosts)
            {
                if (host->is_idle())
                {
                    add_state_to_switch(host->id, host->pstate_asleep);
                    host->machine_state = SWITCHING_OFF;
                }
            }

        /***********************************************************************
         * Tell to batsim to update the machine states
         **********************************************************************/
        update_machine_states(date);
    }

    /* make_decisions from superclass, will in particular take care of sending
     * feedback about job status to the users, if broker enabled */
    DynScheduler::make_decisions(date, update_info, compare_info);
}

void BinPackingEnergy::bin_packing_algo(double date)
{
    /* Binpacking main algo:
     * Go through jobs in decreasing number of requested cores (DescendingSizeOrder) 
     * and hosts in our custom order (~ increasing available core) */
    listofHosts.sort(HostComparator());
    bool host_found = true;
    int previous_job_requested_resources = ((*(_queue->begin()))->job)->nb_requested_resources;
    for (auto job_it = _queue->begin(); job_it != _queue->end();)
    {
        const Job *job = (*job_it)->job;

        /* Optimization part, can be removed if it created bugs
         * If the previous job had the same size as the considered job
         * and didn't find a host we skip the bin-packing step for this job
         * If the previous job was of size 1 then that means that all host are full so
         * we end the algorithm */
        if(!(host_found) && (previous_job_requested_resources == job->nb_requested_resources)){
            if(previous_job_requested_resources==1){
                return;
            }
            job_it++;
            continue;
        }
        
        /* Find the smallest host where it fits */
        host_found = false;
        for (auto host : listofHosts)
        {
            if (host->machine_state == SWITCHING_OFF)
            {
                /* We don't prebook jobs on switching off machines: we
                let them complete their shut down. Consequently, we've
                reached the end of the queue of potentially useful
                hosts. */
                break;
            }

            if (host->nb_available_cores >= job->nb_requested_resources)
            {
                host_found = true;
                execute_or_book(job, host, date);

                /* Even if the jobs are not executed straight away
                (booked) we update the available cores to avoid double
                booking */
                host->nb_available_cores -= job->nb_requested_resources;
                PPK_ASSERT_ERROR(host->nb_available_cores >= 0);

                listofHosts.sort(HostComparator());

                // debug:
                LOG_F(1, "makedecisions, list of hosts=%s",
                    hosts_to_string(listofHosts).c_str());

                break;
            }
        }

        previous_job_requested_resources = job->nb_requested_resources;
        if (host_found)
            job_it = _queue->remove_job(job);
        else
            job_it++;
    }

}
void BinPackingEnergy::add_state_to_switch(int host_id, int pstate)
{
    if (states_to_switch.count(pstate) == 1)
        states_to_switch[pstate].insert(host_id);

    else
        states_to_switch[pstate] = IntervalSet(host_id);
}

void BinPackingEnergy::execute_or_book(
    const Job *job, BinPackingEnergy::SortableHost *host, double date)
{
    /**
     * Execute the job on the host, handling the differences depending on
     * the machine state of the host.
     */
    switch (host->machine_state)
    {
    case AWAKE:
        // Actually execute the job
        mapping = std::vector<int>(job->nb_requested_resources, 0);
        _decision->add_execute_job(
            job->id, IntervalSet(host->id), date, mapping);
        if (broker_enabled)
            broker->update_status_if_dyn_job(job->id, RUNNING);

        current_allocations[job->id] = host;
        break;

    case SWITCHING_ON:
        // The machine is booting up, the job cannot be executed straight
        // away. Book it for later
        host->book(job->id);
        break;

    case ASLEEP:
        // Book the job and ask for bootup
        host->book(job->id);

        add_state_to_switch(host->id, host->pstate_awake);
        host->machine_state = SWITCHING_ON;
        break;

    case SWITCHING_OFF:
        PPK_ASSERT_ERROR(
            false, "Try to schedule a job on a machine that is switching off.");
        break;
    }
}

void BinPackingEnergy::update_machine_states(double date)
/* Only send the new machine states to batsim. The rest will be done when
 * receiving the ack from batsim (on_machine_state_change) */
{
    for (auto mit = states_to_switch.begin(); mit != states_to_switch.end();)
    {
        _decision->add_set_resource_state(mit->second, mit->first, date);

        mit = states_to_switch.erase(mit);
    }
}
