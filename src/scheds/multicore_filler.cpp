#include "multicore_filler.hpp"

#include <loguru.hpp>

// #include "../decision.hpp"
// #include "../json_workload.hpp"
#include "../pempek_assert.hpp"
#include "users/dynscheduler.hpp"
#include "json_workload.hpp"
#include <list>
#include <map>
#include <string>

MulticoreFiller::MulticoreFiller(Workload *workload,
    SchedulingDecision *decision, Queue *queue, ResourceSelector *selector,
    double rjms_delay, rapidjson::Document *variant_options)
    : DynScheduler(
        workload, decision, queue, selector, rjms_delay, variant_options)
{
}

MulticoreFiller::~MulticoreFiller()
{
}

void MulticoreFiller::on_simulation_start(
    double date, const rapidjson::Value &batsim_config)
{
    /* Call superclass. If broker enabled, submit jobs date=0. */
    DynScheduler::on_simulation_start(date, batsim_config);

    _machines.insert(IntervalSet::ClosedInterval(0, _nb_machines - 1));
    PPK_ASSERT_ERROR(_machines.size() == (unsigned int)_nb_machines);
}

void MulticoreFiller::on_simulation_end(double date)
{
    (void)date;
}

// Defined in the super class:
// void MulticoreFiller::on_requested_call(double date){}

void MulticoreFiller::make_decisions(double date,
    SortableJobOrder::UpdateInformation *update_info,
    SortableJobOrder::CompareInformation *compare_info)
{
    /* Code base taken from sequencer.cpp
     * This algorithm executes all the jobs, one after the other.
     * All executors of the jobs are scheduled in the first machine,
     * to test the multicore management.
     * At any time, either 0 or 1 job is running on the platform.
     * The order of the sequence depends on the queue order. */

    // Up to one job finished since last call.
    PPK_ASSERT_ERROR(_jobs_ended_recently.size() <= 1);
    if (!_jobs_ended_recently.empty())
    {
        PPK_ASSERT_ERROR(_isJobRunning);
        _isJobRunning = false;
    }

    // Add valid jobs into the queue
    for (const std::string &job_id : _jobs_released_recently)
    {
        const Job *job = (*_workload)[job_id];

        if (true)
            // we never reject a job, we always try to schedule it on single
            // machine
            _queue->append_job(job, update_info);
        else
        {
            _decision->add_reject_job(job->id, date);
            if (broker_enabled)
                broker->update_status_if_dyn_job(job->id, KILLED);
        }
    }

    // Sort queue if needed
    _queue->sort_queue(update_info, compare_info);

    // Execute the first job on the first machine, thanks to custom mapping
    const Job *job = _queue->first_job_or_nullptr();
    if (job != nullptr && !_isJobRunning)
    {
        std::vector<int> mapping(job->nb_requested_resources, 0);
        _decision->add_execute_job(
            job->id, IntervalSet(first_machine), date, mapping);
        if (broker_enabled)
            broker->update_status_if_dyn_job(job->id, RUNNING);
        _isJobRunning = true;
        _queue->remove_job(job);
    }

    /* make_decisions from superclass, will in particular take care of sending
     * feedback about job status to the users, if broker enabled */
    DynScheduler::make_decisions(date, update_info, compare_info);
}