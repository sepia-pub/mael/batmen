#pragma once

#include "../users/dynscheduler.hpp"

#include <map>
#include <vector>

#include "../locality.hpp"
#include <intervalset.hpp>

class Workload;
class SchedulingDecision;

class MulticoreFiller : public DynScheduler
{
public:
    MulticoreFiller(Workload *workload, SchedulingDecision *decision, Queue *queue, ResourceSelector *selector,
        double rjms_delay, rapidjson::Document *variant_options);

    virtual ~MulticoreFiller();

    virtual void on_simulation_start(double date, const rapidjson::Value &batsim_config);

    virtual void on_simulation_end(double date);

    virtual void make_decisions(double date, SortableJobOrder::UpdateInformation *update_info,
        SortableJobOrder::CompareInformation *compare_info);


private:
    bool _debug = true;
    IntervalSet _machines;
    int first_machine = 0;
    bool _isJobRunning = false;
};
