#pragma once

#include "../users/dynscheduler.hpp"

#include <list>
#include <map>
#include <vector>

#include "../locality.hpp"
#include <intervalset.hpp>

class Workload;
class SchedulingDecision;

/**
 * Represents a multicore SimGrid host and keep track of the number
 * of cores currently in use.
 */
struct SortableHost
{
    const int id;
    const int nb_cores;
    int nb_available_cores;

    SortableHost(int, int);
    void update_available_cores(int nb_cores_available);
};

/**
 * Comparaison fonction for two hosts to allow sorting.
 */
struct HostComparator
{
    bool operator()(SortableHost *host1, SortableHost *host2);
};

/**
 * In this scheduling algorithm, each job is scheduled on only one multicore
 * host. The scheduler tries to have as few hosts as possible that are powered
 * on by packing all the jobs on the least number of hosts and switching off the
 * others.
 */
class BinPacking : public DynScheduler
{
public:
    BinPacking(Workload *workload, SchedulingDecision *decision, Queue *queue,
        ResourceSelector *selector, double rjms_delay,
        rapidjson::Document *variant_options);

    virtual ~BinPacking();

    virtual void on_simulation_start(
        double date, const rapidjson::Value &batsim_config);

    virtual void on_simulation_end(double date);

    virtual void make_decisions(double date,
        SortableJobOrder::UpdateInformation *update_info,
        SortableJobOrder::CompareInformation *compare_info);

private:
    // list of hosts, to be sorted with listofHosts.sort(HostComparator())
    std::list<SortableHost *> listofHosts;
    std::map<std::string, SortableHost *> current_allocations;

    bool _debug = true;
    // to_string fonction, for debugging
    std::string hosts_to_string(std::list<SortableHost *> listofHosts);
};
