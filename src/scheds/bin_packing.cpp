#include "bin_packing.hpp"

#include <iostream>

#include <loguru.hpp>

#include "../pempek_assert.hpp"
#include "scheds/HARD_DEFINED_VAR.hpp"
#include "json_workload.hpp"

SortableHost::SortableHost(int id_host, int cores)
    : id(id_host)
    , nb_cores(cores)
{
    nb_available_cores = nb_cores;
}

void SortableHost::update_available_cores(int nb_cores_available)
{
    nb_available_cores = nb_cores_available;
}

bool HostComparator::operator()(SortableHost *host1, SortableHost *host2)
{
    if (host1->nb_available_cores == host2->nb_available_cores)
        return host1->id < host2->id;
    return host1->nb_available_cores < host2->nb_available_cores;
}

BinPacking::BinPacking(Workload *workload, SchedulingDecision *decision,
    Queue *queue, ResourceSelector *selector, double rjms_delay,
    rapidjson::Document *variant_options)
    : DynScheduler(
        workload, decision, queue, selector, rjms_delay, variant_options)
{
}

BinPacking::~BinPacking()
{
    for (auto host : listofHosts)
        delete host;
    listofHosts.clear();
}

std::string BinPacking::hosts_to_string(std::list<SortableHost *> listofHosts)
{
    std::string s = "{";
    for (auto host : listofHosts)
    {
        s += "(id=" + std::to_string(host->id)
            + ", a_core=" + std::to_string(host->nb_available_cores) + "),";
    }
    s += "}";
    return s;
}

void BinPacking::on_simulation_start(
    double date, const rapidjson::Value &batsim_config)
{
    /* Call superclass. If broker enabled, submit jobs date=0. */
    DynScheduler::on_simulation_start(date, batsim_config);

    // TODO: send an error if --enable-compute-sharing option is not given to
    // batsim CLI. An error like:
    // PPK_ASSERT_ERROR(batsim_config["dynamic-jobs-enabled"].GetBool(),
    //        "This algorithm only works if dynamic job are enabled!");

    for (int i = 0; i < _nb_machines; i++)
    {
        listofHosts.push_back(new SortableHost(i, _nb_cores_per_machine));
    }
    if (_debug)
        LOG_F(1, "binpacking, list of hosts=%s",
            hosts_to_string(listofHosts).c_str());
}

void BinPacking::on_simulation_end(double date)
{
    (void)date;
}

void BinPacking::make_decisions(double date,
    SortableJobOrder::UpdateInformation *update_info,
    SortableJobOrder::CompareInformation *compare_info)
{
    // Update the available machines
    for (const std::string &ended_job_id : _jobs_ended_recently)
    {
        const Job *ended_job = (*_workload)[ended_job_id];
        SortableHost *host = current_allocations[ended_job_id];
        host->nb_available_cores += ended_job->nb_requested_resources;
        PPK_ASSERT_ERROR(host->nb_available_cores <= host->nb_cores);

        current_allocations.erase(ended_job_id);
    }

    // Handle recently released jobs
    for (const std::string &new_job_id : _jobs_released_recently)
    {
        const Job *new_job = (*_workload)[new_job_id];

        if (new_job->nb_requested_resources > _nb_cores_per_machine)
        {
            _decision->add_reject_job(new_job_id, date);
            if (broker_enabled)
                broker->update_status_if_dyn_job(new_job_id, KILLED);
        }
        else
            _queue->append_job(new_job, update_info);
    }

    // Sort jobs in decreasing number of requested cores (DescendingSizeOrder)
    // and hosts in increasing number of available cores
    _queue->sort_queue(update_info, compare_info);
    listofHosts.sort(HostComparator());

    // Go through the job queue and try to schedule the jobs
    for (auto job_it = _queue->begin(); job_it != _queue->end();)
    {
        const Job *job = (*job_it)->job;

        // Find the smaller host where it fits
        bool host_found = false;
        for (auto host : listofHosts)
        {
            if (host->nb_available_cores >= job->nb_requested_resources)
            {
                host_found = true;

                // Schedule the job
                std::vector<int> mapping(job->nb_requested_resources, 0);
                _decision->add_execute_job(
                    job->id, IntervalSet(host->id), date, mapping);
                if (broker_enabled)
                    broker->update_status_if_dyn_job(job->id, RUNNING);

                // Update host state
                current_allocations[job->id] = host;
                host->nb_available_cores -= job->nb_requested_resources;
                PPK_ASSERT_ERROR(host->nb_available_cores >= 0);
                listofHosts.sort(HostComparator());
                if (_debug)
                    LOG_F(1, "binpacking, list of hosts=%s",
                        hosts_to_string(listofHosts).c_str());

                break;
            }
        }

        if (host_found)
            job_it = _queue->remove_job(job);
        else
            job_it++;
    }

    /* make_decisions from superclass, will in particular take care of sending
     * feedback about job status to the users, if broker enabled */
    DynScheduler::make_decisions(date, update_info, compare_info);
}