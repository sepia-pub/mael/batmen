#pragma once

#include "../users/dynscheduler.hpp"

#include <list>
#include <map>
#include <vector>

#include <boost/algorithm/string.hpp>

#include "../locality.hpp"
#include <intervalset.hpp>

class Workload;
class SchedulingDecision;

/**
 * This scheduling algorithm is a version of BinPacking where the scheduler
 * switches off machines when they are idle.
 *
 * The order for the bin packing algorithm is:
 * - jobs in descending order of number of resources asked
 * - awake hosts in ascending order of available cores
 * The hosts switching on or powered off are put at the end of the host queue,
 * in this order. They are still bookable by the jobs.
 */
class BinPackingEnergy : public DynScheduler
{
public:
    enum MachineState
    {
        // Sorted from the earliest available to the latest
        AWAKE,
        SWITCHING_ON,
        ASLEEP,
        SWITCHING_OFF
    };

    /**
     * Represents a multicore SimGrid host and keeps track of the number
     * of cores currently in use and the state of the machine.
     * If the machine is asleep or switching on, the host keeps a list of jobs
     * to execute as soon as the machine is awake.
     */
    class SortableHost
    {
    public:
        const int id;
        const int nb_cores;
        int nb_available_cores;
        MachineState machine_state;
        std::list<std::string> *booked_jobs_id;
        const int pstate_awake, pstate_asleep;
        SortableHost(int id_host, int cores, MachineState state, int awake_pst, int asleep_pst);

    public:
        void update_available_cores(int nb_cores_available);
        void book(std::string job_id);
        bool is_idle();
    };

    /**
     * Comparaison fonction for two hosts to allow sorting.
     */
    struct HostComparator
    {
        bool operator()(SortableHost *host1, SortableHost *host2);
    };

    BinPackingEnergy(Workload *workload, SchedulingDecision *decision, Queue *queue, ResourceSelector *selector,
        double rjms_delay, rapidjson::Document *variant_options);

    virtual ~BinPackingEnergy();

    virtual void on_simulation_start(double date, const rapidjson::Value &batsim_config);
    virtual void on_simulation_end(double date);
    virtual void make_decisions(double date, SortableJobOrder::UpdateInformation *update_info,
        SortableJobOrder::CompareInformation *compare_info);
    virtual void bin_packing_algo(double date);

protected:
    void execute_or_book(const Job *job, SortableHost *host, double date);
    void update_machine_states(double date);

private:
    // list of hosts, to be sorted with listofHosts.sort(HostComparator())
    std::list<SortableHost *> listofHosts;
    std::vector<SortableHost *> vectorofHosts;
    std::map<std::string, SortableHost *> current_allocations;
    std::vector<int> mapping;

    std::map<int, IntervalSet> states_to_switch;
    void add_state_to_switch(int host_id, int pstate);
    bool reached_end_of_simulation = false;
    bool on_simulation_begin = false;

    // to_string fonctions, for debugging
    std::string machine_state_to_string(BinPackingEnergy::MachineState);
    std::string hosts_to_string(std::list<SortableHost *> listofHosts);
};
