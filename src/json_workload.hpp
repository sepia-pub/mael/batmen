#pragma once

#include <map>

#include <boost/format.hpp>
#include <rapidjson/document.h>

#include "exact_numbers.hpp"
#include "rapidjson/error/error.h"
#include <intervalset.hpp>
#include <list>
#include <string>
#include <utility>

#include "scheds/HARD_DEFINED_VAR.hpp"

struct JobAlloc;
class Session;

#define DATE_NEVER -1
#define DATE_UNKNOWN -2

enum JobStatus
{
    WAITING,
    RUNNING,
    FINISHED,
    KILLED
};

struct Job
{
    std::string id;
    int nb_requested_resources;
    std::string profile;
    Rational walltime;
    bool has_walltime = true;
    double submission_time = 0;
    double completion_time = -1;
    mutable std::map<Rational, JobAlloc *> allocations;
    JobStatus status;
    Session *session = nullptr; // for SABjson
};

struct JobAlloc
{
    Rational begin;
    Rational end;
    bool started_in_first_slice;
    bool has_been_inserted = true;
    const Job *job;
    IntervalSet used_machines;
};

struct JobComparator
{
    bool operator()(const Job *j1, const Job *j2) const;
    bool operator()(Job j1, Job j2) const;
};

struct JobSubmissionOrder
{
    JobComparator jobcmp;
    bool operator()(const Job *j1, const Job *j2) const;
};

struct Profile
{
    std::string name;
    std::string workload_name;
    std::string description;
};

class Session
{
public:
    Session(unsigned long id, long first_submit_time, unsigned long nb_jobs);
    ~Session();

    unsigned long id;
    unsigned long unfinished_jobs;
    long start_time; /* in seconds (rounded up to the nearest)
            The submit time of the first job in this session, **as in the replay
            with feedback**. Also used to store the projected start time. */
    long original_first_submit_time; /* the submit time of the first job in this
                                    session, **as in the original trace** */
    std::list<const Job *> jobs;
    std::map<Session *, double> preceding_sessions; /* pair (Session, t_time) */
    std::set<Session *> following_sessions;
};

struct SessionComparator
{
    bool operator()(const Session *s1, const Session *s2) const;
};

/* Map of all jobs, for keeping track in the scheduler */
class Workload
{
public:
    ~Workload();

    Job *operator[](std::string jobID);
    const Job *operator[](std::string jobID) const;
    int nb_jobs() const;

    void set_rjms_delay(Rational rjms_delay);

    void add_job_from_json_object(const rapidjson::Value &object,
        const std::string &job_id, double submission_time);
    void add_job_from_json_description_string(const std::string &json_string,
        const std::string &job_id, double submission_time);

    Job *job_from_json_description_string(const std::string &json_string);
    Job *job_from_json_object(const rapidjson::Value &object);

private:
    void put_file_into_buffer(const std::string &filename);
    void deallocate();

private:
    char *_fileContents = nullptr;
    std::map<std::string, Job *> _jobs;
    Rational _rjms_delay = 0;
    int _job_number = 0;
};

/* List of jobs to submit for users, sorted by submission time */
class UserSubmitQueue
{
public: 
    UserSubmitQueue(
        const std::string user_name, const std::string input_json);
    ~UserSubmitQueue();

    /* Insert the job in the sorted queue */
    void insert_sorted(const Job *job);

    /* Give a pointer to the first elem */
    const Job *top() const;

    /* Remove and give a pointer to the first elem */
    const Job *pop();

    bool is_empty() const;

private:
    std::list<Job *> _jobs;
    JobSubmissionOrder submission_order;
};

/**
 * Useful parsers
 */
struct Parser
{
    static rapidjson::Document parse_input_json(const std::string input_json);

    static Job *job_from_json_object(
        const std::string user_name, const rapidjson::Value &object);
    // with format 'user-name!jobID'
    static std::string json_description_string_from_job(const Job *job);
    static Session *session_from_json_object(const std::string user_name,
        const rapidjson::Value &object, std::list<unsigned long> *prec_sessions,
        std::list<unsigned long> *tt_after_prec_sessions);

    static Session *session_graph_from_SABjson(
        const std::string user_name, const std::string input_json);
    static std::list<Profile *> profile_list_from_input_json(
        const std::string user_name, const std::string input_json);

    static void profile_from_duration(Profile *profile,
        const std::string duration, const std::string user_name);
};