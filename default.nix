{ kapack ? import
    (fetchTarball "https://github.com/oar-team/nur-kapack/archive/master.tar.gz")
  {}
, doCheck ? false
, doCoverage ? true
, batsim ? kapack.batsim
, batexpe ? kapack.batexpe
, python ? kapack.python3
, pythonPackages ? kapack.pkgs.python3Packages
}:

let
  pkgs = kapack.pkgs;
  buildPythonPackage = pythonPackages.buildPythonPackage;

  jobs = rec {
    # Batmen executable file (built from local sources)
    batmen = kapack.batsched.overrideAttrs (attr: rec {
      src = pkgs.lib.sourceByRegex ./. [
        "^src"
        "^src/.*\.?pp"
        "^src/scheds"
        "^src/scheds/.*\.?pp"
        "^src/scheds"
        "^src/scheds/.*\.?pp"
        "^src/users"
        "^src/users/.*\.?pp"
        "^src/external"
        "^src/external/.*\.?pp"
        "^meson\.build"
      ];
      mesonFlags = []
        ++ pkgs.lib.optional doCoverage [ "-Db_coverage=true" ];
      nativeBuildInputs = with kapack; [pkgs.meson pkgs.ninja pkgs.pkg-config
        pkgs.boost pkgs.gmp pkgs.rapidjson intervalset loguru pkgs.cppzmq pkgs.zeromq];
      # Debug build, without any Nix stripping magic.
      mesonBuildType = "debug";
      hardeningDisable = [ "all" ];
      dontStrip = true;
    });

    # Later version of batsim, with memleak bug fixed
    # batsim_latest = kapack.batsim.overrideAttrs (attr: rec{
    #   name = "batsim";
    #   version = "e3dbc246c5e3acd117a84909d351b94bfb6e4a59"; # commit "code: prevent temporary memleak for zmq msgs"
    #   src = fetchgit rec {
    #     url = "https://framagit.org/batsim/batsim.git";
    #     rev = version;
    #     sha256 = "sha256-+SGdNrPjRTGInrEQyFMCWcPIESD5yS+Enc1WRDCdKbQ=";
    #   };
    # });

    # Batmen integration tests.
    test = pkgs.mkShell rec {
      shellHook = ''
        export PATH="./build:$PATH"
      ''; # to make batmen executable available in the env
      buildInputs = with pythonPackages; [
        batsim
        batexpe
        pytest 
        pytest-html # for html export of test results
        pandas
      ];
    };

    test_rebuild = pkgs.mkShell rec {
      buildInputs = with pythonPackages; [
        batmen 
        batsim
        batexpe
        pytest 
        pytest-html
        pandas
      ];
    };

    doc = pkgs.stdenv.mkDerivation rec {
      name = "sphinx-documentation";
      buildInputs = with pythonPackages; [ sphinx sphinx_rtd_theme myst-parser ];
    };

  };


in
  jobs
