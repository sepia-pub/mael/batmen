# Create a new dynamic scheduler

For now, if someone wants to create a new dynamic scheduler (or transform a static scheduler into dynamic), here is what one should do:

- change super class from `ISchedulingAlgorithm` to `DynScheduler`
- add the call `DynScheduler::on_simulation_start(date, batsim_config);` at the beginning of `on_simulation_start`
- add the call `DynScheduler::make_decisions(date, update_info, compare_info);` at the end of `make_decisions`
- update the status of jobs with `if (broker_enabled) broker->update_status_if_dyn_job(new_job_id, STATUS);` each time the scheduler adds or rejects a job

See for example [this commit](https://gitlab.irit.fr/sepia-pub/mael/batmen/-/commit/5b4e30a631c7544ad009b2368512e78b79ffa5b9).
