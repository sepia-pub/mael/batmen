# Build the documentation
The present documentation is located in the folder `docs` and hosted on [readthedocs](https://batmen.readthedocs.io/en/latest/).
It is automatically built and updated whenever there is a push on Batmen's `master` branch.

To build the doc manually, we provide a dedicated nix shell.
From Batmen root directory:

```shell
cd docs/
nix-shell -A doc
make html
```

The doc is generated inside the folder `docs/_build` and can be viewed on the browser by opening the file `docs/_build/html/index.html`. 