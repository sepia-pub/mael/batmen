# User simulation
The interaction with users happens through a "broker".
The broker manages a pool of simulated users and dynamically register new jobs to submit.
These jobs come on top of the input workload read by Batsim. 

Implementation, in the folder `src/users`:

- class `Broker`: implement the broker, submitting the jobs on behalf of the users
- class `DynScheduler`: superclass managing the interaction with the broker from the scheduler
- class `User`: an individual user, submitting her jobs to the broker

## Types of user

For now, we have developed three types of users.

- *modeled users* (class `User`): using a model to generate the jobs to submit
- *replay users* (class `ReplayUser`): replaying a workload trace given as input
- *feedback users* (class `FeedbackUser`): taking feedback on the status of previous jobs into account when submitting the next one 
