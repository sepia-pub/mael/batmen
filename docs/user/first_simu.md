# Run your first simulation

Follow the [steps to install Batmen](./installation.md).
The executable is at the location `build/batmen`.
We suppose that you also have [Batsim] installed on your machine (follow the installation guide [here](https://batsim.readthedocs.io/en/latest/installation.html#installation)).

To run a simulation, we will need **two terminals**:
- one (Batmen) to simulate the users and the scheduler, and 
- one (Batsim) to simulate the underlying infrastructure

Create the output directory:

```bash
mkdir -p out/first_simu
```

Launch Batmen with 
- the scheduler FCFS
- one simulated user (described in the user description file `test/schedconf/routine_greedy_simple.json`) 

```bash
build/batmen -v fcfs \
    --variant_options_filepath test/schedconf/routine_greedy_simple.json
```

Launch Batsim with
- the platform file `test/platforms/monocore/2machines.xml`
- an empty workload file `test/workloads/static/empty.json` (jobs are submitted dynamically by the simulated user)
- the output directory
- all the options required for Batmen to work properly (see their description in [Batsim CLI](https://batsim.readthedocs.io/en/latest/cli.html))  

```bash
path/to/batsim -p test/platforms/monocore/2machines.xml \
    -w test/workloads/static/empty.json \
    -e out/first_simu/ \
    --energy --enable-compute-sharing \
    --enable-dynamic-jobs --acknowledge-dynamic-jobs \
    --enable-profile-reuse
```

After the execution, you should see the [Batsim outputs](https://batsim.readthedocs.io/en/latest/tuto-result-analysis/tuto.html) in the output directory.