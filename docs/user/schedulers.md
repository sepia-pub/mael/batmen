# Available schedulers

Dynamic submission is supported by all the schedulers inheriting from the class `DynScheduler`
i.e., all the schedulers located in the folder `src/scheds`.
Some of them support multicore machines while others only support monocore machines. 

- monocore schedulers:
  - *fcfs*: jobs scheduled in their order of arrival ("first come first served")
  - *easy-bf*: job scheduled in fcfs order, with possibility to execute ("backfill") small jobs that were submitted after if they don't postpone the starting time of the next job
- multicore schedulers:
  - *bin_packing*: the scheduler packs the jobs on the least number of multicore machines. Each job runs on only one host but can take several cores
  - *bin_packing_energy*: same as *bin_packing*, but saves power by switching off the machines as soon as they are idle and powering them on again when needed
  - *multicore_filler*: for test purposes. Schedules only one job at a time on only one multicore host.

For guidance on how to add a new dynamic scheduler, refer to the [dedicated documentation](../dev/scheduler.md).