# Installation
## With Nix (recommanded)
For portability and reproducibility purposes, all the dependencies needed to compile Batmen are managed with [Nix] package manager.
If you don't Nix on your machine, you can install it by following [their documentation](https://nixos.org/download.html).

Once you have Nix installed, go to the root of the project:

```bash
nix-shell -A batmen # enter a Nix environment with all dependencies managed
meson build # generate a build directory
ninja -C build # compile the project into the build directory
```

## Manually
TBD...



[Nix]: https://nixos.org/nix/