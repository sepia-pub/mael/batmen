.. Batmen documentation master file, created by
   sphinx-quickstart on Tue Jun 18 10:38:15 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Batmen's documentation!
==================================

Batmen is a plugin to the scientific simulator `Batsim <https://batsim.org/>`_.
It allows the simulation of **users** (bat-men) of large-scale distributed systems.
Batmen is originally a fork of `batsched <https://framagit.org/batsim/batsched>`_.

How does is work?
-----------------

.. image:: ../batmen_diagram.svg
   :alt: Batmen diagram

A simulation with Batmen has to be launched in parallel with Batsim.

* Batsim takes care of **simulating the IT platform** (job arrival, job termination, energy consumed...), thanks to the underlying simulator `SimGrid <https://simgrid.org/>`_.
* Batmen simulates the **users of the system**, through their **job submission behavior**. It also simulates the **scheduler**.

Batmen was developped mainly by Maël Madon during his PhD thesis.
The most up-to-date description can be found in Chapter II of the `thesis manuscript <https://cloud.irit.fr/s/ZjafxKO0YnaMqpK>`_ (and more precisely in Section II.2.2).
It was used to carry scientific experimental campains, leading to several peer-reveiwed publications:

* `Europar'22 <https://doi.org/10.1007/978-3-031-12597-3_4>`_: Maël Madon, Georges Da Costa, and Jean-Marc Pierson. *"Characterization of Different User Behaviors for Demand Response in Data Centers"*
* `FGCS'24 <https://doi.org/10.1016/j.future.2024.01.024>`_: Maël Madon, Georges Da Costa, and Jean-Marc Pierson. *"Replay with Feedback: How Does the Performance of HPC System Impact User Submission Behavior?"*
* ICT4S'24: Jolyne Gatt, Maël Madon, and Georges Da Costa. *"Digital Sufficiency Behaviors to Deal with Intermittent Energy Sources in a Data Center"*

If you use Batmen for your work, please cite one of these.

This present documentation is divided in two parts, user and developer manual:


.. toctree::
   :maxdepth: 1
   :caption: User manual

   user/installation.md
   user/first_simu.md
   user/user_categories.md
   user/schedulers.md

.. toctree::
   :maxdepth: 1
   :caption: Developer manual

   dev/user_simu.md
   dev/scheduler.md
   dev/tests.md
   dev/documentation.md